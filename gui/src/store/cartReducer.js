import ACTIONS from "./actions";
import _ from "lodash";
import { getEmptyCart } from "../general/functions";

const setTotal = (state) => {
    let total = state.products.map((item) => item.amount * item.price).reduce((prev, curr) => prev + curr, 0);
    state.total = Number.parseFloat(total.toFixed(2));
    return state;
}

const defaultState = {
    id: "",
    creationdate: "",
    userid: "",
    products: [],
    total: 0,
};

/**
 * Description. Cart reducer that implements all dispatch functions relevant for the cart state. 
 */
const cartReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.SET_CART: {
            let newState = action.payload ? action.payload : getEmptyCart();
            localStorage.setItem('cart', JSON.stringify(newState));
            return setTotal(newState);
        }

        case ACTIONS.Types.ADD_TO_CART: {
            let itemProduct = action.payload;
            let newState = _.cloneDeep(state);
            let indexOf = newState.products.findIndex(item => item.productId === itemProduct.productId);
            console.log(indexOf)
            if (indexOf > -1) {
                newState.products[indexOf].amount += 1;
            } else {
                newState.products.push(itemProduct);
            }
            localStorage.setItem('cart', JSON.stringify(newState));
            return setTotal(newState);
        }

        case ACTIONS.Types.REMOVE_FROM_CART: {
            let itemProduct = action.payload;
            let newState = _.cloneDeep(state);
            let indexOf = newState.products.findIndex(item => item.productId === itemProduct.productId);
            console.log(indexOf)
            if (indexOf > -1) {
                newState.products.splice(indexOf,1);
            } 
            localStorage.setItem('cart', JSON.stringify(newState));
            return setTotal(newState);
        }

        case ACTIONS.Types.CHANGE_AMOUNT: {
            let itemProduct = action.payload;
            let newState = _.cloneDeep(state);
            let indexOf = newState.products.findIndex(item => item.productId === itemProduct.productId);
            if (indexOf > -1) {
                if (action.amount === 0) {
                    newState.products.splice(indexOf, 1);
                } else newState.products[indexOf].amount = action.amount;
            }
            return setTotal(newState);
        }

        default:
            return state;
    }
};

export default cartReducer;