import {decodeToken, deleteToken, storeToken} from '../general/functions';

import ACTIONS from './actions';
import _ from 'lodash';

const defaultState = {
    firstname: null,
    editingAddress: false,
};

/**
 * Description. Account reducer that implements all dispatch functions relevant for the account state. 
 * Account state contains information about user's first name.
 */
function accountReducer (state = defaultState, action) {
    switch (action.type) {
        case ACTIONS.Types.LOGIN: {
            storeToken(action.user.token);
            let newState = _.cloneDeep(state);
            let user = decodeToken(action.user.token);
            if (!('firstname' in user) || !user.firstname) {
                newState.firstname = 'User';
            }else {
                newState.firstname = user.firstname;
            }
            return newState;
        }
        case ACTIONS.Types.LOGOUT: {
            deleteToken();
            let newState = _.cloneDeep(state);
            newState.firstname = null;
            return newState;
        }
        case ACTIONS.Types.CHANGE_FIRSTNAME: {
            let newState = _.cloneDeep(state);
            newState.firstname = action.firstname;
            return newState;
        }
        case ACTIONS.Types.SET_EDITING_ADDRESS: {
            let newState = _.cloneDeep(state);
            newState.editingAddress = action.editingAddress;
            return newState;
        }
        default:
            return state;
    }
}

export default accountReducer;