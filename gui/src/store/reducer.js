import accountReducer from './accountReducer';
import cartReducer from './cartReducer';
import { combineReducers } from 'redux';
import paymentReducer from './paymentReducer';
import productReducer from './productReducer';
import { reducer as reduxFormReducer } from 'redux-form';

/**
 * Description. The main reducer that collects all other reducers and combines them into one.
 */
const reducer = combineReducers({
  account: accountReducer,
  cart: cartReducer,
  product: productReducer,
  payment: paymentReducer,
  form: reduxFormReducer, // comes from user account forms
});

export default reducer;