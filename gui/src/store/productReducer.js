import ACTIONS from "./actions";
import _ from "lodash";

const defaultState = { 
    products: [] 
};

/**
 * Description. Products reducer that implements all dispatch functions relevant for the product state. 
 */
const productReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.SET_PRODUCTS: {
            let products = action.products;
            let newState = _.cloneDeep(state);
            newState.products = products;
            return newState;
        }

        default:
            return state;
    }
};

export default productReducer;