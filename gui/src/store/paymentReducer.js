import ACTIONS from "./actions";
import _ from "lodash";

const defaultState = { 
    address: null,
    paymentStatus: 'success',
    shipmentStatus: 'processing',
    paymentSuccess: false,
};

/**
 * Payment reducer for storing and handling shared data between components
 */
const paymentReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.SET_ADDRESS: {
            let address = action.address;
            let newState = _.cloneDeep(state);
            newState.address = address;
            return newState;
        }
        case ACTIONS.Types.SET_PAYMENT_STATUS: {
            let paymentStatus = action.status;
            let newState = _.cloneDeep(state);
            newState.paymentStatus = paymentStatus;
            return newState;
        }
        case ACTIONS.Types.SET_SHIPMENT_STATUS: {
            let shipmentStatus = action.status;
            let newState = _.cloneDeep(state);
            newState.shipmentStatus = shipmentStatus;
            return newState;
        }
        case ACTIONS.Types.SET_PAYMENT_SUCCESS: {
            let paymentSuccess = action.success;
            let newState = _.cloneDeep(state);
            newState.paymentSuccess = paymentSuccess;
            return newState;
        }

        default:
            return state;
    }
};

export default paymentReducer;