/**
 * Description. All types for every reducer's functions.
 */
const Types = {
  // Cart
  ADD_TO_CART: "ADD_TO_CART",
  SET_CART: "SET_CART",
  CHANGE_AMOUNT: "CHANGE_AMOUNT",
  REMOVE_FROM_CART: "REMOVE_FROM_CART",

  // USER
  LOGIN: "LOGIN",
  LOGOUT: "LOGOUT",
  CHANGE_FIRSTNAME: "CHANGE_FIRSTNAME",
  SET_EDITING_ADDRESS: "SET_EDITING_ADDRESS",

  // PRODUCT
  SET_PRODUCTS: "SET_PRODUCTS",

  // PAYMENT
  SET_ADDRESS: "SET_ADDRESS",
  SET_PAYMENT_STATUS: "SET_PAYMENT_STATUS",
  SET_SHIPMENT_STATUS: "SET_SHIPMENT_STATUS",
  SET_PAYMENT_SUCCESS: "SET_PAYMENT_SUCCESS"
};

// actions

// CART
const addToCart = (product, amount) => ({
  type: Types.ADD_TO_CART,
  payload: product
});


const setCart = (cart) => ({
  type: Types.SET_CART,
  payload: cart
});

const changeAmount = (item, amount) => ({
  type: Types.CHANGE_AMOUNT,
  payload: item,
  amount: amount
});

const removeFromCart = (item) => ({
  type: Types.REMOVE_FROM_CART,
  payload: item
});

// ACCOUNT
const login = (user) => ({
  type: Types.LOGIN,
  user
});

const logout = () => ({ type: Types.LOGOUT });

const changeFirstname = (firstname) => ({
  type: Types.CHANGE_FIRSTNAME,
  firstname
});

const setEditingAddress = (editingAddress) => ({
  type: Types.SET_EDITING_ADDRESS,
  editingAddress
});

//PRODUCT
const setProducts = (products) => ({
  type: Types.SET_PRODUCTS,
  products
});

//PAYMENT
const setAddress = (address) => ({
  type: Types.SET_ADDRESS,
  address
});

const setPaymentStatus = (status) => ({
  type: Types.SET_PAYMENT_STATUS,
  status
});

const setShipmentStatus = (status) => ({
  type: Types.SET_SHIPMENT_STATUS,
  status
});

const setPaymentSuccess = (success) => ({
  type: Types.SET_PAYMENT_SUCCESS,
  success
});

export default {
  setCart,
  addToCart,
  changeAmount,
  removeFromCart,
  login,
  logout,
  changeFirstname,
  setEditingAddress,
  setProducts,
  setAddress,
  setPaymentStatus,
  setShipmentStatus,
  setPaymentSuccess,
  Types
};