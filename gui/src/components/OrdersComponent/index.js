import '../Common.css';

import { CircularProgress, List, Typography } from '@material-ui/core';
import { fetchIsTokenValid, getToken, getUserId, renderError } from '../../general/functions';

import Order from './Order';
import React from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

/**
 * Description. User's orders. Not visible if not authorized (logged in).
 */
class OrdersComponent extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isAuthorized: false,
      error: false,
      order: []
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  /**
   * Description. Check authorization. If authorized fetch orders
   */
  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    let orders = [];
    if (isAuthorized) {
      let userId = getUserId();
      orders = await this.getOrdersByUser(userId)
        .then((response) => {
          if (response && response.data) {
            console.log(response.data);
            return response.data;
          }
          error = true;
          return [];
        })
        .catch((err) => {
          console.log(err);
          error = true;
          return [];
        });
    }
    this.setState({
      isAuthorized: isAuthorized,
      isLoading: false,
      error: error,
      orders: orders
    });
  }

  async getOrdersByUser(userId) {
    return axios({
      method: "get",
      url: `/orderservice/byUserId/${userId}`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      params: {
        token: getToken()
      }
    })
  }

  render() {
    // We are still receiving authorization status
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    // error
    if (this.state.error) {
      return renderError('Oh no, something went wrong. We cannot get your orders at the moment. Please try again later.');
    }
    // We are not logged in
    if (!this.state.isAuthorized) {
      return <Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />;
    }
    return (
      <div className='margin-15'>
        <Typography variant='h4' align='left' gutterBottom>
          Your orders
        </Typography>
        <List className='orders-list'>
          {this.state.orders &&
            this.state.orders.map((item, index) => {
              return (
                <Order item={item} key={index} />
              );
            })}
        </List>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OrdersComponent));
