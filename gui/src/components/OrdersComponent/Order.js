import '../Common.css';

import { CircularProgress, Grid, Paper, Typography } from '@material-ui/core';
import { getToken, getUserId, renderSimpleError } from '../../general/functions';

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import moment from 'moment';

/**
 * Order Component
 */
class Order extends React.Component {

  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      error: false,
      shipment_status: null,
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let shipment_status = await axios
      .post(`/api/get_shipment/${getUserId()}/${this.props.item.id}`, {
        token: getToken(),
      })
      .then((response) => {
        if (response && response.data) {
          return response.data;
        }
        error = true;
        return null;
      })
      .catch((err) => {
        console.log(err);
        error = true;
        return null;
      });
    this.setState({
      isLoading: false,
      error: error,
      shipment_status: shipment_status
    });
  }

  getItemCount(products){
    return products.reduce((a,b) => a + (b.amount || 0), 0);
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return renderSimpleError('Oh no, something went wrong. We cannot get your order at the moment. Please try again later.');
    }
    let order = this.props.item;
    return (
      <Paper key={'order_' + this.props.index} elevation={2} className='margin-15'>
        <Grid container spacing={2} alignContent='space-around'>
          <Grid item xs={12} sm container>
            <Grid item xs container direction='column' spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant='subtitle1'>
                  Order from: {moment(order.creationDate).format('MMMM Do YYYY')}
                </Typography>
                <Typography variant='body2' gutterBottom></Typography>
                <Typography variant='body2' color='textSecondary'>
                  Order number: {order.id}
                </Typography>
                <Typography>
                  Shipment status: {this.state.shipment_status.status}
                </Typography>
                <Typography>
                  Shipment last updated: {moment(this.state.shipment_status.datetime).format('MMMM Do YYYY HH:mm:ss')}
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant='subtitle1'>{order.products ? this.getItemCount(order.products) : '0'} items </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Order);
