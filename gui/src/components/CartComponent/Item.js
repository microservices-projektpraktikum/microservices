import '../Common.css';

import { Button, FormControl, InputLabel, MenuItem, Paper, Select, Typography } from '@material-ui/core';
import { arrayBufferToBase64, getUserId } from '../../general/functions';

import ACTIONS from '../../store/actions';
import React from 'react';
import { changeAmount } from './CartRestActions';
import { connect } from 'react-redux';

/**
 * Cart Item Component
 */
const amounts = Array.from(new Array(100),(val, index) => index + 1);
class Item extends React.Component {

  handleAmountChange = (event, item) => {
    let userid = getUserId();
    userid ? this.changeAmountAndSetCart(item, userid, event.target.value) : this.props.changeAmount(item, event.target.value);
  };

  changeAmountAndSetCart = async (item, userid, amount) => {
    let that = this;
    await changeAmount(item, userid, amount)
      .then((response) => this.props.setCart(response))
      .catch((error) => {
        console.log(error);
        let msg = 'message' in error ? error.message : JSON.stringify(error);
        that.props.history.push({ pathname: '/500', state: { msg: msg } });
        return;
      });
  };

  removeItemFromCart = (item) => {
    let userid = getUserId();
    userid ? this.changeAmountAndSetCart(item, userid, 0) : this.props.removeItemFromCart(item);
  };

  render() {
    let item = this.props.item;
    let product = this.props.products.find((product) => product.id.toString() === item.productId.toString());
    if (!product) {
      return <div></div>;
    }
    return (
      <Paper key={item.productId} className='cart-card' elevation={2}>
        <img className='cart-image' src={'data:image/jpg;base64,' + arrayBufferToBase64(product.image.data)} alt={item.name} height={200} />
        <div className='cart-content'>
          <Typography variant='h5' align='center'>
            {product.name}
          </Typography>
          <p className='cart-item-description'>{product.description}</p>
          <div className='cart-item-buttons'>
            <FormControl style={{ width: '60%' }} variant='outlined'>
              <InputLabel id='item-amount'>Amount</InputLabel>
              <Select value={item.amount > 100 ? '100' : item.amount } onChange={(event) => this.handleAmountChange(event, item)} label='Amount'>
                {amounts.map((num) => {
                  return (
                    <MenuItem key={item.productId + num} value={num}>
                      {num}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <Button style={{ margin: '10px' }} color='primary' onClick={() => this.removeItemFromCart(item)}>
              Remove
            </Button>
            <Typography align='right' className='cart-product-price'>
              Price: {Number.parseFloat(product.price * item.amount).toFixed(2)}€
            </Typography>
          </div>
        </div>
      </Paper>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  products: state.product.products,
});

const mapDispatchToProps = (dispatch) => ({
  changeAmount: (item, amount) => dispatch(ACTIONS.changeAmount(item, amount)),
  removeItemFromCart: (item) => dispatch(ACTIONS.removeFromCart(item)),
  setCart: (cart) => dispatch(ACTIONS.setCart(cart)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Item);
