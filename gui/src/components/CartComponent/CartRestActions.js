import axios from 'axios';
import { getToken, getEmptyCart } from '../../general/functions';

const URL = '/cartservice';

export function getCartForUser(userid) {
  return axios({
    method: 'get',
    url: `${URL}/byUserId/last/${userid}`,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    params: {
      token: getToken()
    }
  })
    .then((response) => {
      if (response && response.data) {
        return response.data;
      } else return getEmptyCart();
    });
}

export function addToCart(product, userid) {
  var data = JSON.stringify(product);
    return axios({
      method: "post",
      url: `${URL}/addtocart`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      data,
      params: {
        userid, 
        token: getToken()
      }
    })
      .then(response => {
        if (response && response.data) {
          console.log(response);
          return response.data;
        };
      });
  }

  export function removeFromCart(productid, amount, userid) {
    return axios({
      method: "post",
      url: `${URL}/removefromcart`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      params: {
        productid,
        amount,
        userid,
        token: getToken()
    }
    })
      .then(response => {
        if (response && response.data) {
          localStorage.setItem('cart', JSON.stringify(response.data));
          return response.data;
        };
      })
      .catch(error => console.log(error));
  }

  export function changeAmount(product, userid, amount) {
    var data = JSON.stringify(product);
      return axios({
        method: "put",
        url: `${URL}/changeAmount`,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        data,
        params: {
          userid,
          amount,
          token: getToken()
        }
      })
        .then(response => {
          if (response && response.data) {
            console.log(response);
            localStorage.setItem('cart', JSON.stringify(response.data));
            return response.data;
          } else return null;
        });
    }

    export function saveCart(cart) {
      let data = JSON.stringify(cart);
      return axios({
        method: "post",
        url: `${URL}/`,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        data,
        params: {
          token: getToken()
        }
      })
        .then(response => {
          if (response && response.data) {
            console.log(response);
            localStorage.setItem('cart', JSON.stringify(response.data));
            return response.data;
          } else return null;
        })
        .catch(error => console.log(error));
    }
