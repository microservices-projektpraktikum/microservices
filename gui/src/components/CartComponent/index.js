import '../Common.css';

import { Button, CircularProgress, Tooltip, Typography } from '@material-ui/core';
import { createCart, fetchIsTokenValid, getProducts, renderError } from '../../general/functions';

import ACTIONS from '../../store/actions';
import Item from './Item';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class CartComponent extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isAuthorized: false,
      error: false,
    };
    this.redirectToLogin = this.redirectToLogin.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    createCart(this.props.setCart)
      .catch(error => {
        this.props.history.push({ pathname: '/500', state: { msg: error.message } });
      });
    await getProducts().then(response => {
      this.props.setProducts(response);
    });
    this.setState({
      isAuthorized: isAuthorized,
      isLoading: false,
      error: error,
    });
  }

  redirectToLogin() {
    this.props.history.push('/login');
  }

  handlePlaceOrder = () => {
    // not logged in
    if (!this.state.isAuthorized) {
      return this.redirectToLogin();
    }
    if (this.props.cart.total === 0) {
      return;
    }
    this.props.history.push('/payment');
  };

  render() {
    // still receiving authorization status and data
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    // error
    if (this.state.error) {
      return renderError('Oh no, something went wrong. We cannot process your cart at the moment. Please try again later.');
    }
    return this.renderCart();
  }

  renderItems() {
    let itemList = this.props.cart && this.props.cart.products.map((item, index) => {
      return <Item item={item} key={index} />;
    });
    return <div className='cart-box'>{this.props.products && <div> {itemList} </div>}</div>
  }

  renderCart() {
    return (
      <div className='container'>
        <h3 className='center'>Cart</h3>
        {this.renderItems()}
        <div className='margin-15'>
          <Typography variant='h5' align='right' className='margin-15'>
            {' '}
            Total: {this.props.cart.total}€{' '}
          </Typography>
        </div>
        <div>
          <Tooltip title='Place Order' aria-label='place-order' className='place-order-button'>
            <Button color='primary' variant='contained' type='button' onClick={() => this.handlePlaceOrder()}>
              Place Order
            </Button>
          </Tooltip>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  products: state.product.products,
});

const mapDispatchToProps = (dispatch) => ({
  setProducts: products => dispatch(ACTIONS.setProducts(products)),
  setCart: cart => dispatch(ACTIONS.setCart(cart))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CartComponent));
