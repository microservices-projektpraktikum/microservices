import '../Common.css';

import { Alert, AlertTitle } from '@material-ui/lab';
import { Redirect, withRouter } from 'react-router-dom';

import React from "react";

/**
 * Description. Error page for resolving network errors to inform the user that something went wrong.
 */
class ErrorComponent extends React.Component {
    render() {
        if (this.props.history.location.state && this.props.history.location.state.msg) {
            return (
                <div className="all-center">
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {this.props.history.location.state.msg}
                    </Alert>
                </div>
            );
        }
        return (
            <Redirect to={{ pathname: '/', state: { from: this.props.location } }} />
        )
    }
}

export default withRouter(ErrorComponent);