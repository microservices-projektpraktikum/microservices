import '../Common.css';

import { Alert, AlertTitle } from '@material-ui/lab';
import { CircularProgress, ClickAwayListener, Grow, IconButton, MenuItem, MenuList, Paper, Popper, Tooltip } from '@material-ui/core';
import React, { createRef } from 'react';
import { fetchIsTokenValid, getEmptyCart, getToken, getUserId } from '../../general/functions';

import ACTIONS from '../../store/actions';
import { AccountCircle } from '@material-ui/icons';
import { Button } from '@material-ui/core';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

/**
 * Description. Popping in and out account menu.
 */
class AccountMenuForm extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      open: false,
      anchorRef: createRef(),
      error: false,
    };
    this.logout = this.logout.bind(this);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.redirectToAccount = this.redirectToAccount.bind(this);
    this.redirectToLogin = this.redirectToLogin.bind(this);
    this.redirectToOrders = this.redirectToOrders.bind(this);
  }

  /**
   * Description. Check authorization. If authorized, fetch user's first name for a greeting.
   */
  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    if (isAuthorized) {
      this.fetchFirstname();
    }
    if (this._isMounted) {
      this.setState({
        isLoading: false,
        error: error,
      });
    }
  }

  /**
   * Description. Prevent all functions from changing state for an unmounted component.
   *
   * Note: Functions should explicitly check if the component is unmounted.
   */
  componentWillUnmount() {
    this._isMounted = false;
  }

  async fetchFirstname() {
    let user_id = getUserId();
    if (!user_id) {
      return null;
    }
    let data = {
      token: getToken(),
    };
    let error = false;
    let firstname = await axios.post(`/firstname/${user_id}`, data)
      .then((response) => response.data.firstname)
      .catch((err) => {
        console.log(err);
        error = true;
        return null;
      });
    if (!firstname || firstname.trim().length === 0) {
      firstname = 'User';
    }
    if (this._isMounted) {
      if (!error) {
        this.props.changeFirstname(firstname);
      }
      this.setState({
        error: error,
      });
    }
  }

  async sendLogout() {
    let data = { token: getToken() };
    return axios.post('/logout', data);
  }

  logout() {
    this.sendLogout();
    this.props.logout();

    let cart = getEmptyCart();
    this.props.setCart(cart);

    this.props.history.push('/');
  }

  handleMenuClick() {
    this.setState({ open: !this.state.prevOpen });
    this.fetchFirstname();
  }

  handleMenuClose = (event) => {
    if (this.state.anchorRef.current & this.state.anchorRef.current.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };

  redirectToAccount() {
    this.props.history.push('/account');
  }

  redirectToOrders() {
    this.props.history.push('/orders');
  }

  redirectToLogin() {
    this.props.history.push('/login');
  }

  render() {
    // We are still receiving authorization status
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    return (
      <div className='MuiButtonBase-root MuiIconButton-root navbar-button'>
        <Tooltip title='Account' arial-label='show account'>
          <IconButton className='navbar-button' ref={this.state.anchorRef} aria-controls={this.state.open ? 'menu-list-grow' : undefined} aria-haspopup='true' onClick={this.handleMenuClick}>
            <AccountCircle />
            Account
          </IconButton>
        </Tooltip>
        <Popper open={this.state.open} anchorEl={this.state.anchorRef.current} role={undefined} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow {...TransitionProps} style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}>
              {this.renderMenu()}
            </Grow>
          )}
        </Popper>
      </div>
    );
  }

  renderMenu() {
    if (this.state.error) {
      return (
        <Paper className='account-menu' color='secondary' variant='elevation' elevation={5}>
          <ClickAwayListener onClickAway={this.handleMenuClose}>
            <div className="account-error-div">
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Oh no, something went wrong. We cannot get your data at the moment. Please try again later.
              </Alert>
            </div>
          </ClickAwayListener>
        </Paper>
      );
    }
    return (
      <Paper className='account-menu' color='secondary' variant='elevation' elevation={5}>
        {this.renderGreeting()}
        <ClickAwayListener onClickAway={this.handleMenuClose}>
          <MenuList autoFocusItem={this.state.open} id='menu-list-grow' onClick={this.handleMenuClose}>
            <MenuItem key='accountMenu' onClick={this.redirectToAccount}>
              Account
            </MenuItem>
            <MenuItem key='ordersMenu' onClick={this.redirectToOrders}>
              Orders
            </MenuItem>
            {this.renderLogout()}
          </MenuList>
        </ClickAwayListener>
      </Paper>

    );
  }

  renderGreeting() {
    // Logged in
    if (this.props.account.firstname) {
      return <div className='account-greeting'>Hi, {this.props.account.firstname}</div>;
    }
    return (
      <div className='account-new-greeting'>
        <Button variant='contained' size='small' color='primary' onClick={this.redirectToLogin}>
          Login
        </Button>
        <div className='account-msg'>
          Don't have an account?
          <br />
          <span className='register-link' onClick={() => this.props.history.push('/register')}>
            Register here
          </span>
        </div>
      </div>
    );
  }

  renderLogout() {
    if (this.props.account.firstname) {
      return (
        <MenuItem key='logoutMenu' onClick={this.logout}>
          Logout
        </MenuItem>
      );
    }
    return null;
  }
}

const mapStateToProps = (state) => ({
  account: state.account,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(ACTIONS.logout()),
  changeFirstname: (firstname) => dispatch(ACTIONS.changeFirstname(firstname)),
  setCart: cart => dispatch(ACTIONS.setCart(cart))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AccountMenuForm));
