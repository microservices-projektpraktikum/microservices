import '../Common.css';

import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, CircularProgress } from '@material-ui/core';
import { Field, SubmissionError, propTypes, reduxForm } from 'redux-form';
import { Redirect, withRouter } from 'react-router-dom';
import { createCart, fetchIsTokenValid, handleFormError, renderField } from '../../general/functions';

import ACTIONS from '../../store/actions';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import sha256 from 'crypto-js/sha256';

export async function sendLogin(user) {
  if (!('email' in user) || !('password' in user) || !user.email || !user.password) {
    throw new SubmissionError({
      _error: 'Your account email or password is incorrect',
    });
  }
  let data = {
    email: user.email,
    password: sha256(user.password).toString(),
  };
  return axios
    .post('/login', data)
    .then((response) => response.data)
    .catch((error) => {
      if ('response' in error && error.response.status === 401) {
        throw new SubmissionError({ _error: 'Invalid user name or password.' });
      }
      handleFormError(error, 'Authentification', 'login');
    });
}

/**
 * Description. Login form of a user.
 */
class LoginForm extends React.Component {
  _isMounted = false;

  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isAuthorized: false,
      error: false,
    };
  }

  /**
   * Description. Check authorization.
   */
  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    if (this._isMounted) {
      this.setState({
        isLoading: false,
        isAuthorized: isAuthorized,
        error: error,
      });
    }
  }

  /**
   * Description. Prevent all functions from changing state for an unmounted component.
   *
   * Note: Functions should explicitly check if the component is unmounted.
   */
  componentWillUnmount() {
    this._isMounted = false;
  }

  handleLogin = async (user) => {
    let login = await sendLogin(user);
    this.props.login(login);
    let error = await createCart(this.props.setCart).catch(err => {
      console.log(JSON.stringify(err));
      return true;
    });
    if (this._isMounted && error) {
      this.setState({
        error: true
      });
      return;
    }
    // Login successful. Redirect to home.
    this.props.history.push('/');
  };

  render() {
    // We are still receiving authorization status
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    // There was an error with authorization
    if (this.state.error) {
      return this.renderError();
    }
    // Already logged in
    if (this.state.isAuthorized) {
      return <Redirect to={{ pathname: '/', state: { from: this.props.location } }} />;
    }
    const { error, handleSubmit, submitting } = this.props;
    return (
      <div className='form-card'>
        <form className='form' onSubmit={handleSubmit(this.handleLogin)}>
          <Field name='email' type='email' label='Email Address' component={renderField} />
          <Field name='password' type='password' label='Password' component={renderField} />
          {error && <Alert severity='warning'>{error}</Alert>}
          <div className='account-btn-div'>
            <Button color='primary' variant='contained' type='submit' disabled={submitting}>
              Login
            </Button>
          </div>
        </form>
        <div className='mt-2'>
          <span>Don't have an account? </span>
          <span className='loginText' onClick={() => this.props.history.push('/register')}>
            Register here
          </span>
        </div>
      </div>
    );
  }

  renderError() {
    return (
      <div className='form-card'>
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          Oh no, something went wrong. We cannot get your data at the moment. Please try again later.
        </Alert>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    account: state.account,
    cart: state.cart
});

const mapDispatchToProps = dispatch => ({
    login: (user) => dispatch(ACTIONS.login(user)),
    setCart: cart => dispatch(ACTIONS.setCart(cart))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'loginValidation',
  })(withRouter(LoginForm))
);
