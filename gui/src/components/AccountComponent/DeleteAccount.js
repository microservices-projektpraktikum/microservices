import '../Common.css';

import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, CircularProgress } from '@material-ui/core';
import { getEmptyCart, getToken, getUserId, renderSimpleError } from '../../general/functions';

import ACTIONS from '../../store/actions';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { styled } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

const AccountButton = styled(Button)({
  'margin-right': '20px',
});

class DeleteAccount extends React.Component {
  _isMounted = false;
  
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoading: true,
      deleteAccount: false,
      deleteAccountMsg: null,
    };
    this.deleteAccount = this.deleteAccount.bind(this);
    this.cancelDeleteAccount = this.cancelDeleteAccount.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      isLoading: false,
    });
  }

  cancelDeleteAccount() {
    if (this._isMounted) {
      this.setState({
        deleteAccount: false,
        deleteAccountMsg: null,
      });
    }
  }

  async deleteAccount() {
    if (!this.state.deleteAccount) {
      if (this._isMounted) {
        this.setState({
          deleteAccount: !this.state.deleteAccount,
        });
        return;
      }
    }
    let data = {
      token: getToken(),
    };
    if (this._isMounted) {
      this.setState({
        isLoading: true,
      });
    }
    return axios
      .delete(`delete/${getUserId()}`, { data: data })
      .then((response) => {
        if (response.status === 200) {
          this.props.logout();
          let cart = getEmptyCart();
          this.props.setCart(cart);
          if (this._isMounted) {
            this.setState({
              isLoading: false,
            });
          }
          this.props.history.push('/');
        }
      })
      .catch((err) => {
        console.log(err.response.data);
        if (this._isMounted) {
          this.setState({
            isLoading: false,
            deleteAccountMsg: "Failed to delete account. Please try again later.",
          });
        }
      });
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return renderSimpleError('Oh no, something went wrong. We cannot delete your account at the moment. Please try again later.');
    }
    return this.renderDeleteAccount();
  }

  renderDeleteAccount() {
    if (this.state.deleteAccount) {
      return (
        <div className='delete-account-div'>
          {this.renderDeleteMessage()}
          <div className='account-btn-div'>
            <AccountButton color='primary' variant='contained' type='button' onClick={this.deleteAccount}>
              Delete Account
            </AccountButton>
            <Button color='primary' variant='contained' type='button' onClick={this.cancelDeleteAccount}>
              Cancel
            </Button>
          </div>
        </div>
      );
    }
    return (
      <div className='delete-account-div'>
        <Button color='primary' variant='contained' type='button' onClick={this.deleteAccount}>
          Delete Account
        </Button>
      </div>
    );
  }

  renderDeleteMessage() {
    if (!this.state.deleteAccountMsg) {
      return (
        <Alert severity="warning">
          <AlertTitle>Warning</AlertTitle>
          Are you sure you want to delete your account? This cannot be undone.
        </Alert>
      )
    }
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {this.state.deleteAccountMsg}
      </Alert>
    )
  }
}

const mapStateToProps = (state) => ({ });

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(ACTIONS.logout()),
  setCart: (cart) => dispatch(ACTIONS.setCart(cart)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(DeleteAccount));
