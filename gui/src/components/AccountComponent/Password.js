import '../Common.css';

import { Button, CircularProgress } from '@material-ui/core';
import { Field, propTypes, reduxForm } from 'redux-form';
import { getToken, getUserId, renderField } from '../../general/functions';

import { Alert } from '@material-ui/lab';
import React from 'react';
import { SubmissionError } from 'redux-form';
import axios from 'axios';
import { connect } from 'react-redux';
import sha256 from 'crypto-js/sha256';

class Password extends React.Component {
  _isMounted = false;

  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      passwordChanged: false,
    };
    this.changePassword = this.changePassword.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      isLoading: false,
    });
  }

  validatePassword(user) {
    if (!('password' in user) || !user.password || user.password.length < 6) {
      throw new SubmissionError({ password: 'Incorrect password' });
    }
    if (!('new_password' in user) || !user.new_password || user.new_password.length < 6) {
      throw new SubmissionError({ new_password: 'Password must be at least 6 characters long' });
    }
    if (user.password === user.new_password) {
      throw new SubmissionError({ new_password: 'New password must not be the same as the old password' });
    }
    if (!('repeat_new_password' in user) || !user.repeat_new_password || user.repeat_new_password !== user.new_password) {
      throw new SubmissionError({
        new_password: 'Passwords do not match',
        repeat_new_password: 'Passwords do not match',
      });
    }
  }

  async changePassword(user) {
    this.validatePassword(user);
    let data = {
      token: getToken(),
      old_password: sha256(user.password).toString(),
      new_password: sha256(user.new_password).toString(),
    };
    if (this._isMounted) {
      this.setState({
        isLoading: true,
      });
    }
    return axios
      .post(`change_password/${getUserId()}`, data)
      .then((response) => {
        if (response.status === 200) {
          // Clear the fields
          this.props.change('password', null);
          this.props.change('new_password', null);
          this.props.change('repeat_new_password', null);
          if (this._isMounted) {
            this.setState({
              isLoading: false,
              passwordChanged: true,
            });
          }
        } else {
          if (this._isMounted) {
            this.setState({
              isLoading: false,
            });
          }
          throw new SubmissionError({ _error: 'Could not change the password. Please try again later.' });
        }
      })
      .catch((error) => {
        if (this._isMounted) {
          this.setState({
            isLoading: false,
          });
        }
        if ('response' in error && error.response.status === 401) {
          throw new SubmissionError({ password: 'Invalid password.' });
        }
        throw new SubmissionError({ _error: 'Could not change the password. Please try again later.' });
      });
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    return this.renderPassword();
  }

  renderPassword() {
    const { error, handleSubmit } = this.props;
    return (
      <form className='form'>
        <Field name='password' type='password' label='Current password' component={renderField} />
        <Field name='new_password' type='password' label='New password' component={renderField} />
        <Field name='repeat_new_password' type='password' label='Repeat new password' component={renderField} />
        {error && <Alert severity='warning'>{error}</Alert>}
        {this.state.passwordChanged && <Alert severity='success'> Password was changed successfully!</Alert>}
        <div className='account-btn-div'>
          <Button color='primary' variant='contained' type='button' onClick={handleSubmit(this.changePassword)}>
            Change Password
          </Button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => ({
  initialValues: {
    password: null,
    new_password: null,
    repeat_new_password: null,
  },
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'passwordForm',
    touchOnBlur: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
  })(Password)
);
