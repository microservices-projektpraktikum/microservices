import '../Common.css';

import { Button, CircularProgress } from '@material-ui/core';
import { Field, propTypes, reduxForm } from 'redux-form';
import { getToken, getUserId, renderField, renderSimpleError } from '../../general/functions';

import ACTIONS from '../../store/actions';
import { Alert } from '@material-ui/lab';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

class Address extends React.Component {
  _isMounted = false;
  
  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoading: true,
      profile: null,
      editingAddress: false,
      addressError: null,
    };
    this.toggleAddressEdit = this.toggleAddressEdit.bind(this);
    this.submitAddress = this.submitAddress.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;

    // Fetch address from the user service
    let address = null;
    let error = false;
    address = await this.fetchAddress()
      .then((response) => response.data)
      .catch((err) => {
        console.log(err);
        error = true;
        return null;
      });
    this.props.setAddress(address);

    // Initialize the form with the address values
    this.props.initialize({
      ...address,
    });

    this.setState({
      isLoading: false,
      error: error,
    });
  }

  toggleAddressEdit() {
    let addressError = this.state.addressError;
    if (!this.state.editingAddress) {
      addressError = null;
    }
    this.setState({
      editingAddress: !this.state.editingAddress,
      addressError: addressError,
    });
  }

  async fetchAddress() {
    let data = {
      token: getToken(),
    };
    return axios.post(`/get_address/${getUserId()}`, data);
  }

  getAddressValues(address) {
    let a = {
      fullname: '',
      street: '',
      city: '',
      postcode: '',
      country: '',
      phone: ''
    }
    if (!address) {
      return a;
    }
    for (let key in address) {
      a[key] = address[key];
    }
    return a;
  }

  async submitAddress(address) {
    this.toggleAddressEdit();
    if (this._isMounted) {
      this.setState({
        isLoading: true,
      });
    }
    let addressError = null;
    let data = {
      token: getToken(),
      ...this.getAddressValues(address),
    };
    await axios
      .post(`/set_address/${getUserId()}`, data)
      .then((response) => {
        this.props.setAddress(address);
        return response.data;
      })
      .catch((err) => {
        console.log(err);
        addressError = 'Could not submit address change. Please try again later.';
      });
    if (this._isMounted) {
      this.setState({
        isLoading: false,
        addressError: addressError,
      });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return renderSimpleError('Oh no, something went wrong. We cannot get your address at the moment. Please try again later.');
    }
    return this.renderAddress();
  }

  renderAddress() {
    const { addressError } = this.state;
    return (
      <div className='grid-box'>
        <form className='form'>
          <h2>Shipment address</h2>
          <Field name='fullname' type='text' label='Full Name' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          <Field name='street' type='text' label='Street and house number' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          <Field name='city' type='text' label='City/Town' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          <Field name='postcode' type='text' label='Postcode' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          <Field name='country' type='text' label='Country' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          <Field name='phone' type='text' label='Phone number' component={renderField} props={{ disabled: !this.state.editingAddress }} />
          {addressError && <Alert severity='warning'>{addressError}</Alert>}
          <div className='account-btn-div'>{this.renderAddressButton()}</div>
        </form>
      </div>
    );
  }

  renderAddressButton() {
    if (this.state.editingAddress) {
      return (
        <Button color='primary' variant='contained' type='button' onClick={this.props.handleSubmit(this.submitAddress)}>
          Submit Address
        </Button>
      );
    }
    return (
      <Button color='primary' variant='contained' type='button' onClick={this.toggleAddressEdit}>
        Edit Address
      </Button>
    );
  }
}

const mapStateToProps = (state) => ({
  account: state.account,
  initialValues: {
    fullname: null,
    street: null,
    city: null,
    postcode: null,
    country: null,
    phone: null,
  },
});

const mapDispatchToProps = (dispatch) => ({
  setAddress: (address) => dispatch(ACTIONS.setAddress(address)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'addressForm',
    touchOnBlur: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
  })(Address)
);
