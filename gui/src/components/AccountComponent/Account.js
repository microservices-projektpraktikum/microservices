import '../Common.css';

import { Redirect, withRouter } from 'react-router-dom';

import Address from './Address';
import {CircularProgress} from '@material-ui/core';
import DeleteAccount from './DeleteAccount';
import Password from './Password';
import Profile from './Profile';
import React from 'react';
import { connect } from 'react-redux';
import { fetchIsTokenValid } from '../../general/functions';

/**
 * User's account page.
 */
class AccountComponent extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isAuthorized: false,
      error: false,
      isLoading: true,
    };
  }

  /**
   * Prevent all functions from changing state for an unmounted component.
   *
   * Note: Functions should explicitly check if the component is unmounted.
   */
  componentWillUnmount() {
    this._isMounted = false;
  }

  /**
   * Check authorization.
   */
  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    this.setState({
      isAuthorized: isAuthorized,
      error: error,
      isLoading: false,
    });
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return this.renderError('Oh no, something went wrong. We cannot get your data at the moment. Please try again later.');
    }
    if (this.state.isAuthorized) {
      return (
        <div className='grid-box'>
          <Profile />
          <Address />
          <Password />
          <DeleteAccount />
        </div>
      );
    }
    return <Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />;
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AccountComponent));
