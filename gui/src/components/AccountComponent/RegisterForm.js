import '../Common.css';

import { Button, CircularProgress } from '@material-ui/core';
import { Field, SubmissionError, propTypes, reduxForm } from 'redux-form';
import { Redirect, withRouter } from 'react-router-dom';
import { createCart, fetchIsTokenValid, handleFormError, renderError, renderField } from '../../general/functions';

import ACTIONS from '../../store/actions';
import { Alert } from '@material-ui/lab';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { sendLogin } from './LoginForm';
import sha256 from 'crypto-js/sha256';

/**
 * Description. Registration form of a new user.
 */
class RegisterForm extends React.Component {
  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      isAuthorized: false,
      isLoading: true,
      error: false,
    };
  }

  /**
   * Description. Check authorization.
   */
  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    if (this._isMounted) {
      this.setState({
        isLoading: false,
        isAuthorized: isAuthorized,
        error: error,
      });
    }
  }

  /**
   * Description. Prevent all functions from changing state for an unmounted component.
   *
   * Note: Functions should explicitly check if the component is unmounted.
   */
  componentWillUnmount() {
    this._isMounted = false;
  }

  async sendRegister(user) {
    if (!('email' in user) || !user.email) {
      throw new SubmissionError({ email: 'Not a valid email' });
    }
    if (!('password' in user) || !user.password || user.password.length < 6) {
      throw new SubmissionError({ password: 'Password must be at least 6 characters long' });
    }
    if (!('confirmPassword' in user) || !user.confirmPassword || user.confirmPassword !== user.password) {
      throw new SubmissionError({
        password: 'Passwords do not match',
        confirmPassword: 'Passwords do not match',
      });
    }
    let data = {
      email: user.email,
      password: sha256(user.password).toString(),
    };
    return axios.post('/register', data).catch((error) => {
      if ('response' in error && error.response.status === 409) {
        throw new SubmissionError({ _error: 'Email is already in use.' });
      }
      handleFormError(error, 'User', 'register');
    });
  }

  handleRegister = async (user) => {
    await this.sendRegister(user);
    // Login to receive a token.
    let login = await sendLogin(user);
    this.props.login(login);
    let error = await createCart(this.props.setCart).catch(err => {
      console.log(JSON.stringify(err));
      return true;
    });
    if (this._isMounted && error) {
      this.setState({
        error: true
      });
      return;
    }
    // Registration successful. Redirect to home.
    this.props.history.push('/');
  };

  render() {
    // We are still receiving authorization status
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return renderError("Oh no, something went wrong. We cannot register you at the moment. Please try again later.");
    }
    // Already logged in
    if (this.state.isAuthorized) {
      return <Redirect to={{ pathname: '/', state: { from: this.props.location } }} />;
    }
    const { error, handleSubmit, submitting } = this.props;
    return (
      <div className='form-card'>
        <form className='form' onSubmit={handleSubmit(this.handleRegister)}>
          <Field name='email' type='email' label='Email Address' component={renderField} />
          <Field name='password' type='password' label='Password' component={renderField} />
          <Field name='confirmPassword' type='password' label='Confirm Password' component={renderField} />
          {error && <Alert severity='warning'>{error}</Alert>}
          <div className='account-btn-div'>
            <Button color='primary' variant='contained' type='submit' disabled={submitting}>
              Register
            </Button>
          </div>
        </form>
        <div className='mt-2'>
          <span>Already have an account? </span>
          <span className='loginText' onClick={() => this.props.history.push('/login')}>
            Login here
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  account: state.account,
});

const mapDispatchToProps = (dispatch) => ({
  register: (user) => dispatch(ACTIONS.register(user)),
  login: (user) => dispatch(ACTIONS.login(user)),
  setCart: cart => dispatch(ACTIONS.setCart(cart))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'registrationValidation',
  })(withRouter(RegisterForm))
);
