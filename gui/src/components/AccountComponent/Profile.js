import '../Common.css';

import { Button, CircularProgress } from '@material-ui/core';
import { Field, propTypes, reduxForm } from 'redux-form';
import { getToken, getUserId, renderField } from '../../general/functions';

import ACTIONS from '../../store/actions';
import { Alert } from '@material-ui/lab';
import Moment from 'react-moment';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

class Profile extends React.Component {
  _isMounted = false;

  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoading: true,
      profile: null,
      editingProfile: false,
      profileError: null,
    };
    this.toggleProfileEdit = this.toggleProfileEdit.bind(this);
    this.submitProfile = this.submitProfile.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;

    // Fetch profile from the user service
    let error = false
    let profile = await this.fetchProfile()
      .then((response) => response.data)
      .catch((err) => {
        console.log(err);
        error = true;
        return null;
      });

    this.props.initialize({
      ...profile,
    });

    this.setState({
      isLoading: false,
      error: error,
      profile: profile,
    });
  }

  async fetchProfile() {
    let data = {
      token: getToken(),
    };
    return axios.post(`/get_profile/${getUserId()}`, data);
  }

  toggleProfileEdit() {
    let profileError = this.state.profileError;
    if (!this.state.editingProfile) {
      profileError = null;
    }
    this.setState({
      editingProfile: !this.state.editingProfile,
      profileError: profileError,
    });
  }

  async submitProfile(user) {
    this.toggleProfileEdit();
    if (this._isMounted) {
      this.setState({
        isLoading: true,
      });
    }
    let data = {
      token: getToken(),
      firstname: user.firstname,
      lastname: user.lastname,
    };
    let profileError = null;
    await axios
      .post(`/set_profile/${getUserId()}`, data)
      .then((response) => response.data)
      .catch((err) => {
        console.log(err);
        profileError = 'Could not submit profile change. Please try again later.';
      });

    if (!profileError) {
      this.props.changeFirstname(data.firstname);
    }
    if (this._isMounted) {
      this.setState({
        isLoading: false,
        profileError: profileError,
      });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return this.renderError('Oh no, something went wrong. We cannot get your data at the moment. Please try again later.');
    }
    return this.renderProfile();
  }

  renderProfile() {
    const { profileError } = this.state;
    return (
      <div className='grid-box'>
        <label className='account-label'>Email</label>
        <label className='account-input'>{this.state.profile.email}</label>
        <label className='account-label'>Member since</label>
        <Moment className='account-input' format='YYYY/MM/DD'>
          {this.state.profile.created_on}
        </Moment>
        <form className='form'>
          <h2>Profile</h2>
          <Field name='firstname' type='text' label='First Name' component={renderField} props={{ disabled: !this.state.editingProfile }} />
          <Field name='lastname' type='text' label='Last Name' component={renderField} props={{ disabled: !this.state.editingProfile }} />
          {profileError && <Alert severity='warning'>{profileError}</Alert>}
          <div className='account-btn-div'>{this.renderProfileButton()}</div>
        </form>
      </div>
    );
  }

  renderProfileButton() {
    if (this.state.editingProfile) {
      return (
        <Button color='primary' variant='contained' type='button' onClick={this.props.handleSubmit(this.submitProfile)}>
          Submit Profile
        </Button>
      );
    }
    return (
      <Button color='primary' variant='contained' type='button' onClick={this.toggleProfileEdit}>
        Edit Profile
      </Button>
    );
  }
}

const mapStateToProps = (state) => ({
  initialValues: {
    email: null,
    created_on: null,
    firstname: null,
    lastname: null,
  },
});

const mapDispatchToProps = (dispatch) => ({
  changeFirstname: (firstname) => dispatch(ACTIONS.changeFirstname(firstname)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'profileForm',
    touchOnBlur: false,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true,
  })(Profile)
);
