import { AppBar, Badge, IconButton, Toolbar, Tooltip } from '@material-ui/core';
import { Home, ShoppingCart } from '@material-ui/icons';
import { Link, withRouter } from 'react-router-dom';

import ACTIONS from "../../store/actions";
import AccountMenuForm from "../AccountComponent/AccountMenu";
import React from 'react';
import { connect } from "react-redux";
import { createCart } from '../../general/functions';

/**
 * Description. The top navigation menu bar.
 */
class NavComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.goHome = this.goHome.bind(this);
    }

    componentDidMount() {
        createCart(this.props.setCart)
        .catch(error => {
            this.props.history.push({ pathname: '/500', state: { msg: error.message } });
        });
    }

    goHome() {
        this.props.history.push('/');
    }

    getCartSize = () => {
        let total = 0;
        if (this.props.cart && this.props.cart.products) {
            this.props.cart.products.forEach(item => {
                total += item.amount;
            });
        }
        return total;
    }

    render() {
        return (
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="home" onClick={this.goHome}>
                        <Home />
                    </IconButton>
                    <div className="navbar-buttons">
                        <AccountMenuForm />
                        <Link to="/cart">
                            <Tooltip title="Cart" aria-label="show cart">
                                <IconButton className="navbar-button">
                                    <Badge badgeContent={this.getCartSize()}>
                                        <ShoppingCart />
                                    </Badge>
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </div>
                </Toolbar>
            </AppBar>
        )
    }
}

const mapStateToProps = state => ({
    cart: state.cart
});

const mapDispatchToProps = dispatch => ({
    setCart: (cart) => dispatch(ACTIONS.setCart(cart))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(NavComponent));