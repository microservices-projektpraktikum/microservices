import '../Common.css';

import { CircularProgress, IconButton, Paper, Tooltip, Typography } from '@material-ui/core';
import { arrayBufferToBase64, getProducts, getUserId } from '../../general/functions';

import ACTIONS from "../../store/actions";
import { AddCircle } from '@material-ui/icons';
import React from 'react';
import { addToCart } from '../CartComponent/CartRestActions';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class ProductsComponent extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    this._isMounted = true;
    getProducts().then(response => {
      if (this._isMounted) {
        this.props.setProducts(response);
      };
    });
    this.setState({
      isLoading: false,
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  addItemToCart = (item, amount) => {
    let userid = getUserId();
    console.log(`This is the userid: ${userid}`);
    let product = {
      amount,
      price: item.price,
      productId: item.id.toString()
    }

    userid ? this.addItemAndSetCart(product, userid) : this.props.addItemToCart(product);
  }

  addItemAndSetCart = async (product, userid) => {
    let that = this;
    await addToCart(product, userid).then(response => this.props.setCart(response)).catch(error => {
      console.log(error);
      let msg = 'message' in error ? error.message : JSON.stringify(error);
      that.props.history.push({ pathname: '/500', state: { msg: msg } });
      return;
    });
  }

  render() {
    // still receiving data
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    let itemList = this.props.products && this.props.products.map(item => {
      return (
        <Paper key={item.id} className="product-card">
          <div className="card-image">
            <img className="product-preview" src={"data:image/jpg;base64," + arrayBufferToBase64(item.image.data)} alt={item.name} />
            <Typography variant="h5" align="center" >{item.name}</Typography>
            <Tooltip title="Add item to cart" aria-label="add item cart" className="add-to-cart-button">
              <IconButton color="primary" onClick={() => this.addItemToCart(item, 1)}>
                <AddCircle />
              </IconButton>
            </Tooltip>
          </div>

          <div className="card-content">
            <p>{item.description}</p>
            <p><b>Price: {item.price}€</b></p>
          </div>
        </Paper>

      )
    })

    return (
      <div className="container">
        <h3 className="center">Our items</h3>
        <div className="box">
          {itemList}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  products: state.product.products
});

const mapDispatchToProps = dispatch => ({
  addItemToCart: (product) => dispatch(ACTIONS.addToCart(product)),
  setProducts: products => dispatch(ACTIONS.setProducts(products)),
  setCart: cart => dispatch(ACTIONS.setCart(cart))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProductsComponent));
