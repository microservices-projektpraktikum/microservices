import '../Common.css';

import { Button, CircularProgress } from '@material-ui/core';
import { Field, SubmissionError, propTypes, reduxForm } from 'redux-form';
import { deleteCart, rollbackOrder, rollbackPayment, sendOrderRequest, sendPaymentRequest, sendShipmentRequest } from './PaymentRestActions';
import { getEmptyCart, getToken, getUserId, renderField, renderSimpleError } from '../../general/functions';

import ACTIONS from '../../store/actions';
import { Alert } from '@material-ui/lab';
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

const required = (value) => (value || typeof value === 'number' ? undefined : 'Required');
/**
 * Credit Card Component
 */
class CreditCard extends React.Component {
  _isMounted = false;

  static propTypes = {
    ...propTypes,
  };

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoading: true,
      profile: null,
      editingAddress: false,
      addressError: null,
    }
    this.pay = this.pay.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;

    // Fetch address from the user service
    let address = null;
    let error = false;
    address = await axios.post(`/get_address/${getUserId()}`, { token: getToken() })
      .then((response) => response.data)
      .catch((err) => {
        console.log(err);
        error = true;
        return null;
      });

    // Initialize the form with the address values
    this.props.initialize({
      ...address,
    });

    this.setState({
      isLoading: false,
      error: error,
    });
    this.props.setPaymentSuccess(false);
  }

  setLoading(isLoading) {
    if (this._isMounted) {
      this.setState({
        isLoading: isLoading,
      });
    }
  }

  validateAddress(address) {
    let a = address;
    if (!a) {
      throw new SubmissionError({
        fullname: 'Full name cannot be empty',
        street: 'Street cannot be empty',
        city: 'City cannot be empty',
        postcode: 'Postcode cannot be empty',
        country: 'Country cannot be empty',
        phone: 'Phone cannot be empty'
      });
    }
    let fullnameError, streetError, cityError, postcodeError, countryError, phoneError = null;
    if (!('fullname' in a) || !a.fullname || a.fullname.length < 1) {
      fullnameError = 'Full name cannot be empty';
    }
    if (!('street' in a) || !a.street || a.street.length < 1) {
      streetError = 'Street cannot be empty';
    }
    if (!('city' in a) || !a.city || a.city.length < 1) {
      cityError = 'City cannot be empty';
    }
    if (!('postcode' in a) || !a.postcode || a.postcode.length < 1) {
      postcodeError = 'Postcode cannot be empty';
    }
    if (!('country' in a) || !a.country || a.country.length < 1) {
      countryError = 'Country cannot be empty';
    }
    if (!('phone' in a) || !a.phone || a.phone.length < 1) {
      phoneError = 'Phone cannot be empty';
    }
    if (fullnameError || streetError || cityError || postcodeError || countryError || phoneError){
      throw new SubmissionError({
        fullname: fullnameError,
        street: streetError,
        city: cityError,
        postcode: postcodeError,
        country: countryError,
        phone: phoneError
      });
    }
  }

  pay = async (data) => {
    this.validateAddress(data);
    this.setLoading(true);
    let orderid = null;
    let paymentid = null;
    try {
      orderid = await sendOrderRequest(this.props.cart.id);

      try {
        paymentid = await sendPaymentRequest(
          data.cardnr,
          this.props.cart.total,
          this.props.paymentStatus.toUpperCase()
        );
      } catch (err) {
        await rollbackOrder(orderid);
        throw err;
      }

      try {
        await sendShipmentRequest(
          orderid,
          this.props.shipmentStatus,
          {
            fullname: data.fullname,
            street: data.street,
            city: data.city,
            postcode: data.postcode,
            country: data.country,
            phone: data.phone
          }
        );
      } catch (err) {
        await rollbackPayment(paymentid);
        await rollbackOrder(orderid);
        throw err;
      }
    } catch (err) {
      this.setLoading(false);
      if (err instanceof SubmissionError){
        throw err;
      }
      throw new SubmissionError({
        _error: JSON.stringify(err)
      });
    }

    console.log('Clearing the cart...');
    deleteCart(this.props.cart.id);
    let cart = getEmptyCart();
    this.props.setCart(cart);

    this.setLoading(false);
    this.props.setPaymentSuccess(true);
  };

  render() {
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    if (this.state.error) {
      return renderSimpleError('Oh no, something went wrong. We cannot get your address at the moment. Please try again later.');
    }

    const { error, handleSubmit } = this.props;
    console.log(error);
    return (
      <div>
        <form className='form mw-400' onSubmit={handleSubmit(this.pay)}>
          {this.renderAddress()}
          <h2>Payment</h2>
          <Field validate={[required]} name='cardnr' type='text' label='Enter credit card number' component={renderField} />
          <Field validate={[required]} name='pin' type='text' label='Enter pin' component={renderField} />
          {error && <Alert severity='warning'>{error}</Alert>}
          <div className='total'>Total: {this.props.cart.total}€{' '}</div>
          <div className='account-btn-div'>
            <Button color='primary' variant='contained' type='submit'> Pay </Button>
          </div>
        </form>
      </div>
    );
  }

  renderAddress() {
    const { addressError } = this.state;
    return (
      <div className='grid-box'>
        <h2>Shipment address</h2>
        <Field name='fullname' type='text' label='Full Name' component={renderField} />
        <Field name='street' type='text' label='Street and house number' component={renderField} />
        <Field name='city' type='text' label='City/Town' component={renderField} />
        <Field name='postcode' type='text' label='Postcode' component={renderField} />
        <Field name='country' type='text' label='Country' component={renderField} />
        <Field name='phone' type='text' label='Phone number' component={renderField} />
        {addressError && <Alert severity='warning'>{addressError}</Alert>}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  address: state.payment.address,
  paymentStatus: state.payment.paymentStatus,
  shipmentStatus: state.payment.shipmentStatus,
  initialValues: {
    fullname: null,
    street: null,
    city: null,
    postcode: null,
    country: null,
    phone: null,
  },
});

const mapDispatchToProps = (dispatch) => ({
  setPaymentSuccess: (success) => dispatch(ACTIONS.setPaymentSuccess(success)),
  setCart: (cart) => dispatch(ACTIONS.setCart(cart)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  reduxForm({
    form: 'PaymentForm',
  })(CreditCard)
);
