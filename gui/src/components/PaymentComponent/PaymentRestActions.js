import { getToken, getUserId } from '../../general/functions';

import { SubmissionError } from 'redux-form';
import axios from 'axios';

function handleError(error, service, task) {
  if ('message' in error && error.message === 'Network Error') {
    console.log(JSON.stringify(error));
    throw new SubmissionError({
      _error: `Could not connect to the ${service} server. Please try again later.`,
    });
  }
  if ('response' in error) {
    let resp = error.response;
    if ('data' in resp) {
      console.log(JSON.stringify(resp.data));
      throw new SubmissionError({ _error: JSON.stringify(resp.data) });
    }
    console.log(JSON.stringify(resp));
    throw new SubmissionError({ _error: error.message });
  }
  console.log(JSON.stringify(error));
  throw new SubmissionError({ _error: `Failed to ${task} because of internal problems :(` });
}

export async function sendOrderRequest(cartid) {
  console.log(`Attempting to make an order...${cartid}`);
  let data = {
    cartid: cartid,
    token: getToken(),
  };
  return axios
    .post('/orderservice/placeOrder', data)
    .then((response) => {
      console.log(`Order successful, id: ${response.data.id}`)
      return response.data.id;
    })
    .catch((error) => {
      handleError(error, 'Order', 'place order');
    });
}

export async function sendPaymentRequest(ccNumber, amount, paymentStatus) {
  console.log(`Attempting to pay...: ${ccNumber}, ${amount}, ${paymentStatus}`);
  let data = {
    ccNumber: ccNumber,
    amount: amount,
    paymentResult: paymentStatus,
    token: getToken(),
  };
  return axios
    .post(`/handlePayment`, data)
    .then((response) => {
      console.log(`Payment successful, id: ${response.data.id}`)
      return response.data.id;
    })
    .catch((error) => {
      if ('response' in error && error.response.status === 502) {
        throw new SubmissionError({ _error: 'Transaction failed. Please check your card number and pin.' });
      }
      handleError(error, 'Payment', 'pay');
    });
}

export async function sendShipmentRequest(orderid, shipmentStatus, address) {
  console.log(`Attempting to ship...:${orderid}, ${shipmentStatus}, ${JSON.stringify(address)}`);
  return axios
    .post(`/api/ship/${getUserId()}/${orderid}/${shipmentStatus}`, {
      token: getToken(),
      address
    })
    .then((response) => {
      console.log(`Shipment successful, id: ${response.data.id}`)
      return response.data.id;
    })
    .catch((error) => {
      handleError(error, 'Shipment', 'ship');
    });
}

export async function rollbackPayment(paymentid) {
  console.log(`Attempting to rollback payment:...${paymentid}`);
  return axios
    .post('/deletePaymentById', {
      token: getToken(),
      id: paymentid,
    })
    .then((_) => {
      console.log(`Successfully rolled back payment with ${paymentid}`)
      return true;
    })
    .catch((error) => {
      console.log(`Failed to rollback payment. This is a critical error : ${error}`)
      return false;
    });
}

export async function rollbackOrder(orderid) {
  console.log(`Attempting to rollback order...: ${orderid}`);
  return axios
    .delete(`/orderservice/deleteOrder?id=${orderid}&token=${getToken()}`)
    .then((_) => {
      console.log(`Successfully rolled back order with ${orderid}`)
      return true;
    })
    .catch((error) => {
      console.log(`Failed to rollback order. This is a critical error : ${error}`)
      return false;
    });
}

export async function deleteCart(cartid) {
  console.log(`Attempting to delete cart...: ${cartid}`);
  return axios
    .delete(`/cartservice/deletecart/${cartid}?token=${getToken()}`)
    .then((_) => {
      console.log(`Successfully deleted cart`)
      return true;
    })
    .catch((error) => {
      console.log(`Failed to delete cart. This is a critical error : ${error}`)
      return false;
    });
}