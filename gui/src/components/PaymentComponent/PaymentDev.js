import '../Common.css';

import ACTIONS from '../../store/actions';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/**
 * Payment component in development mode
 */
class PaymentDev extends React.Component {
  constructor(props) {
    super(props);
    this.handleDevPaymentChange = this.handleDevPaymentChange.bind(this);
  }

  handleDevPaymentChange = (event) => {
    let value = event.target.value;
    this.props.setPaymentStatus(value);
  };

  render() {
    return (
      <div className='grid-item'>
        <div className='grid-box'>
          <form className='form'>
            <FormControl component='fieldset'>
              <FormLabel component='legend'>Payment</FormLabel>
              <RadioGroup aria-label='paymentStatus' name='paymentStatus' value={this.props.paymentStatus} onChange={this.handleDevPaymentChange}>
                <FormControlLabel value='success' control={<Radio />} label='Success' />
                <FormControlLabel value='fail' control={<Radio />} label='Fail' />
              </RadioGroup>
            </FormControl>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  paymentStatus: state.payment.paymentStatus,
});

const mapDispatchToProps = (dispatch) => ({
  setPaymentStatus: (status) => dispatch(ACTIONS.setPaymentStatus(status)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'paymentDevForm',
  })(PaymentDev)
);
