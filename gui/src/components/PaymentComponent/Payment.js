import '../Common.css';

import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, CircularProgress } from '@material-ui/core';
import { Redirect, withRouter } from 'react-router-dom';
import { fetchIsTokenValid, renderError } from '../../general/functions';

import ACTIONS from '../../store/actions';
import CreditCard from './CreditCard';
import PaymentDev from './PaymentDev';
import React from 'react';
import ShipmentDev from './ShipmentDev';
import { connect } from 'react-redux';

/**
 * Order-Payment-Shipment Component
 */
class PaymentComponent extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isAuthorized: false,
      error: false,
    };
    this.redirectToHome = this.redirectToHome.bind(this);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    let error = false;
    let isAuthorized = await fetchIsTokenValid().catch((err) => {
      console.log(err);
      error = true;
      return false;
    });
    if (isAuthorized && !this.isStrId(this.props.cart.id)) {
        console.log(this.props);
        console.log(`Cart id is invalid: ${this.props.cart.id}`);
        error = true;
    }
    this.setState({
      isAuthorized: isAuthorized,
      isLoading: false,
      error: error,
    });
  }

  redirectToHome() {
    this.props.setPaymentSuccess(false);
    this.props.history.push('/');
  }

  isStrId(str) {
    return str && str.length !== 0;
  }

  render() {
    // still receiving authorization status and fetching data
    if (this.state.isLoading) {
      return <CircularProgress />;
    }
    // error
    if (this.state.error) {
      return renderError('Oh no, something went wrong. We cannot process your order at the moment. Please try again later.');
    }
    // not logged in
    if (!this.state.isAuthorized) {
      return <Redirect to={{ pathname: '/', state: { from: this.props.location } }} />;
    }
    if (this.props.success) {
      return this.renderSuccess();
    }
    // logged in
    return this.renderPayment();
  }

  renderPayment() {
    return (
      <div className='payment-div'>
        <CreditCard/>
        <div className='h-separator'></div>
        <div className='delete-account-div'>
          <h2>Development Mode For Demonstration Purposes</h2>
          <div className='grid-container'>
            <PaymentDev />
            <div className='v-separator' />
            <ShipmentDev />
          </div>
        </div>
      </div>
    );
  }

  renderSuccess() {
    return (
      <div className='all-center'>
        <div>
          <Alert severity='success'>
            <AlertTitle>Success</AlertTitle>
            You have successfully paid for your order.
          </Alert>
          <Button color='primary' variant='contained' onClick={this.redirectToHome}>
            Continue Shopping
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  success: state.payment.paymentSuccess,
});

const mapDispatchToProps = (dispatch) => ({ 
  setPaymentSuccess: (success) => dispatch(ACTIONS.setPaymentSuccess(success)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PaymentComponent));
