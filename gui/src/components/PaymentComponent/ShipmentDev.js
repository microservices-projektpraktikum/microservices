import '../Common.css';

import ACTIONS from '../../store/actions';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

/**
 * Shipment component in development mode
 */
class ShipmentDev extends React.Component {
  constructor(props) {
    super(props);
    this.handleDevShipmentChange = this.handleDevShipmentChange.bind(this);
  }

  handleDevShipmentChange = (event) => {
    let value = event.target.value;
    this.props.setShipmentStatus(value);
  };

  render() {
    return (
      <div className='grid-item'>
        <div className='grid-box'>
          <form className='form'>
            <FormControl component='fieldset'>
              <FormLabel component='legend'>Shipment</FormLabel>
              <RadioGroup aria-label='shipmentStatus' name='shipmentStatus' value={this.props.shipmentStatus} onChange={this.handleDevShipmentChange}>
                <FormControlLabel value='processing' control={<Radio />} label='Processing' />
                <FormControlLabel value='dispatched' control={<Radio />} label='Dispatched' />
                <FormControlLabel value='transit' control={<Radio />} label='In transit' />
                <FormControlLabel value='delivered' control={<Radio />} label='Delivered' />
                <FormControlLabel value='fail' control={<Radio />} label='Fail' />
              </RadioGroup>
            </FormControl>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  shipmentStatus: state.payment.shipmentStatus,
});

const mapDispatchToProps = (dispatch) => ({
  setShipmentStatus: (status) => dispatch(ACTIONS.setShipmentStatus(status)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'shipmentDevForm',
  })(ShipmentDev)
);
