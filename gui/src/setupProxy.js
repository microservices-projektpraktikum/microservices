const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  // auth
  app.use(
    '/login',
    createProxyMiddleware({
      target: 'http://auth-service:4000',
      changeOrigin: true,
    })
  );
  app.use(
    '/logout',
    createProxyMiddleware({
      target: 'http://auth-service:4000',
      changeOrigin: true,
    })
  );
  app.use(
    '/isTokenValid',
    createProxyMiddleware({
      target: 'http://auth-service:4000',
      changeOrigin: true,
    })
  );
  // user
  app.use(
    '/get_profile',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/set_profile',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/get_address',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/set_address',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/change_password',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/firstname',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/delete',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  app.use(
    '/register',
    createProxyMiddleware({
      target: 'http://user-service:6002',
      changeOrigin: true,
    })
  );
  // order
  app.use(
    '/orderservice',
    createProxyMiddleware({
      target: 'http://order-service:3102',
      changeOrigin: true,
    })
  );
  // cart
  app.use(
    '/cartservice',
    createProxyMiddleware({
      target: 'http://cart-service:3103',
      changeOrigin: true,
    })
  );
  // shipment
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://shipment-service:8000',
      changeOrigin: true,
    })
  );
  // payment
  app.use(
    '/handlePayment',
    createProxyMiddleware({
      target: 'http://payment-service:3200',
      changeOrigin: true,
    })
  );
  app.use(
    '/deletePaymentById',
    createProxyMiddleware({
      target: 'http://payment-service:3200',
      changeOrigin: true,
    })
  );
  // product
  app.use(
    '/getProducts',
    createProxyMiddleware({
      target: 'http://product-service:3100',
      changeOrigin: true,
    })
  );
};
