import './App.css';

import {
  Route,
  BrowserRouter as Router,
  Switch,
} from "react-router-dom";

import AccountComponent from './components/AccountComponent/Account';
import CartComponent from './components/CartComponent';
import ErrorComponent from './components/ErrorComponent/Error';
import LoginForm from './components/AccountComponent/LoginForm';
import NavComponent from './components/NavComponent';
import OrdersComponent from './components/OrdersComponent';
import PaymentComponent from './components/PaymentComponent/Payment';
import ProductsComponent from './components/ProductsComponent';
import React from 'react';
import { Provider as ReduxProvider } from "react-redux";
import RegisterForm from './components/AccountComponent/RegisterForm';
import configureStore from "./store/store";

const reduxStore = configureStore(window.REDUX_INITIAL_DATA);

/**
 * Description. The main component of the GUI application. 
 * Contains all other components, implements Routing.
 */
class App extends React.Component{
  render() {
    return (
      <ReduxProvider store={reduxStore}>
        <div className="App">
          <Router>
              <NavComponent />
              <Switch>
                <Route path="/login">
                  <LoginForm />
                </Route>
                <Route path="/register">
                  <RegisterForm />
                </Route>
                <Route path="/account">
                  <AccountComponent />
                </Route>
                <Route path="/orders">
                  <OrdersComponent/>
                </Route>
                <Route path="/cart">
                  <CartComponent/>
                </Route>
                <Route path="/payment">
                  <PaymentComponent/>
                </Route>
                <Route path="/500">
                  <ErrorComponent/>
                </Route>
                <Route path="/">
                  <ProductsComponent/>
                </Route>
              </Switch>
          </Router>
        </div>
      </ReduxProvider>
    );
  }
}

export default App;