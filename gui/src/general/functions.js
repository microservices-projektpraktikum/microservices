import { Alert, AlertTitle } from '@material-ui/lab';
import { getCartForUser, saveCart } from '../components/CartComponent/CartRestActions';

import React from 'react';
import { SubmissionError } from 'redux-form';
import _ from 'lodash';
import axios from 'axios';
import jwt from 'jwt-decode';

const emptyCart = {
  id: '',
  creationdate: '',
  userid: '',
  products: [],
};

export function arrayBufferToBase64(buffer) {
  var binary = '';
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

export const storeToken = (token) => {
  localStorage.setItem('jwtToken', token);
};

export const decodeToken = (token) => {
  return jwt(localStorage.getItem('jwtToken'));
};

export const getToken = () => {
  return localStorage.getItem('jwtToken');
};

export const deleteToken = () => {
  localStorage.removeItem('jwtToken');
};

export async function fetchIsTokenValid() {
  if ('jwtToken' in localStorage) {
    let data = { token: getToken() };
    return axios.post('/isTokenValid', data).then((response) => response.data.valid);
  }
  return false;
}

export function getUserId() {
  let token = getToken();
  return token ? decodeToken(token).user_id : null;
}

export async function createCart(setCart) {
    let userid = getUserId();
    let currentCart = null;

    if (localStorage.getItem('cart')) {
        currentCart = JSON.parse(localStorage.getItem('cart'));
    }

    if (userid) {
        await getCartForUser(userid).then(response => mergeCarts(currentCart, response, setCart));
        
    } else {
        let cart = _.cloneDeep(emptyCart);
        currentCart != null ? setCart(JSON.parse(localStorage.getItem('cart'))) : setCart(cart);        
    }
}

export function getEmptyCart(){
    return _.cloneDeep(emptyCart);   
}

function mergeCarts(currentCart, oldCart, setCart) {
    if(currentCart && oldCart && currentCart.id !== oldCart.id && currentCart.products && currentCart.products.length > 0){
        currentCart.products.forEach(itemProduct => {
            let indexOf = oldCart.products.findIndex(item => item.productId === itemProduct.productId);
            if (indexOf > -1) {
                oldCart.products[indexOf].amount += itemProduct.amount;
            } else {
                oldCart.products.push(itemProduct);
            }
        });
        saveCart(oldCart);
    }
    setCart(oldCart);
}


export const renderField = ({ type, label, input, meta: { touched, error }, disabled }) => (
  <div>
    <label className='account-label'>{label}</label>
    <div>
      <input {...input} className='field' placeholder={label} type={type} disabled={disabled} />
      {touched && error && <Alert severity='error'>{error}</Alert>}
    </div>
  </div>
);

export function handleFormError(error, service, task) {
  console.log(JSON.stringify(error));
  if ('message' in error && error.message === 'Network Error') {
    throw new SubmissionError({
      _error: `Could not connect to the ${service} server. Please try again later.`,
    });
  }
  if ('response' in error) {
    throw new SubmissionError({ _error: error.message });
  }
  throw new SubmissionError({ _error: `Failed to ${task} because of internal problems :(` });
}

export function renderError(msg) {
  return (
    <div className="all-center">
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {msg}
      </Alert>
    </div>
  );
}

export function renderSimpleError(msg) {
  return (
    <div className="simple-center">
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {msg}
      </Alert>
    </div>
  );
}

export function getProducts() {
  return axios({
    method: 'get',
    url: `/getProducts`,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  })
    .then((response) => {
      if (response && response.data && response.data.rows) {
        console.log(response);
        return response.data.rows;
      } else return [];
    })
    .catch((error) => console.log(error));
}
