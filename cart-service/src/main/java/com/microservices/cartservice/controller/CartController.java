package com.microservices.cartservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.microservices.cartservice.entity.CartEntity;
import com.microservices.cartservice.entity.ProductItemEntity;
import com.microservices.cartservice.repository.CartRepository;
import com.microservices.cartservice.service.CartBusinessImpl;
import com.microservices.cartservice.service.TokenConsumer;
import javax.transaction.Transactional;

import javassist.NotFoundException;

@RestController
@RequestMapping(path = "cartservice")
@CrossOrigin(origins = "*", maxAge = 3600)
@Transactional
public class CartController {
	
	@Autowired
	private CartRepository repository;
	
	@Autowired
	private CartBusinessImpl business;
	
	@Autowired
	private TokenConsumer tokenConsumer;
	
	/*
	 * Controller methods have a request mapping, params, a token check using the token consumer
	 * and a call to the business logic for functionality. 
	 * They either return the response or an unauthorized error if the token is not valid
	 */
	
	@GetMapping("/")
	public Iterable<CartEntity> fetchAll() {
		return repository.findAll();
	}
	
	@PostMapping("/create")
	public CartEntity createCartForUser(@RequestParam String userid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return business.createCart(userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@PostMapping("/")
	public CartEntity saveCart(@RequestBody CartEntity cart, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return business.saveCart(cart);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
		
	}
	
	@PostMapping("/addtocart")
	public CartEntity addToCart(@RequestBody ProductItemEntity product, @RequestParam(name = "userid") String userid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return business.addToCart(product, userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
		
	}
	
	@PostMapping("/removefromcart")
	public CartEntity removeFromCart(@RequestBody ProductItemEntity product, @RequestParam(name = "userid") String userid, @RequestParam(name = "token") String token) throws NotFoundException {
		if (tokenConsumer.isTokenValid(token)) {
			return business.removeFromCart(product, userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@PutMapping("/changeAmount")
	public CartEntity changeAmount(@RequestBody ProductItemEntity product, @RequestParam(name = "userid") String userid, @RequestParam(name = "amount") int amount,
			@RequestParam(name = "token") String token) throws NotFoundException {
		if (tokenConsumer.isTokenValid(token)) {
			return business.changeAmount(product, userid, amount);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@GetMapping("/byUserId/last/{userid}")
	public CartEntity fetchLastCartByUserid(@PathVariable(name ="userid") String userid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return business.findLastByUserid(userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@GetMapping("/byId/{cartid}")
	public CartEntity fetchCartById(@PathVariable(name ="cartid") String cartid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return repository.findById(cartid).get();
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@DeleteMapping("/deletecart/{cartid}")
	public void deleteCart(@PathVariable(name = "cartid") String cartid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			repository.deleteById(cartid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@DeleteMapping("/deletecartbyuser/{userid}")
	public void deleteCartByUserId(@PathVariable(name = "userid") String userid, @RequestParam(name = "token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			repository.deleteByUserid(userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	
}
