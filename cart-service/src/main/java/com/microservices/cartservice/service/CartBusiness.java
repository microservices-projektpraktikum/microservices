package com.microservices.cartservice.service;

import com.microservices.cartservice.entity.CartEntity;
import com.microservices.cartservice.entity.ProductItemEntity;

import javassist.NotFoundException;

public interface CartBusiness {
	
	public CartEntity addToCart(ProductItemEntity product, String userid);
	
	public CartEntity removeFromCart(ProductItemEntity product, String userid) throws NotFoundException;
	
	public CartEntity findLastByUserid(String userid);
}
