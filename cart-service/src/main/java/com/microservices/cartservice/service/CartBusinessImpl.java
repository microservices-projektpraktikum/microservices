package com.microservices.cartservice.service;

import java.util.Optional;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.IntStream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservices.cartservice.entity.CartEntity;
import com.microservices.cartservice.entity.ProductItemEntity;
import com.microservices.cartservice.repository.CartRepository;

import javassist.NotFoundException;

@Service
@Transactional
public class CartBusinessImpl implements CartBusiness {
	
	@Autowired
	private CartRepository repository;
	
	public CartEntity saveCart(CartEntity cart) {
		// Check if there is an existing cart for this user
		Optional<CartEntity> cartOptional = repository.findByUserid(cart.getUserid());
		
		// If it exists then delete it
		if(cartOptional.isPresent()) {
			repository.deleteById(cartOptional.get().getId());
		}
		
		// Save the new cart
		return repository.save(cart);
	}
	
	public CartEntity createCart(String userid) {
		// Check if a user already has a cart
		Optional<CartEntity> cartOptional = repository.findByUserid(userid);
		// Return old cart if it exists
		if(cartOptional.isPresent()) {
			return cartOptional.get();
		}
		// Create a new cart to return and set user id
		CartEntity newCart = new CartEntity();
		newCart.setUserid(userid);
		return repository.save(newCart);
	}

	@Override
	public CartEntity addToCart(ProductItemEntity product, String userid) {
		// Get current cart for this user
		CartEntity cart = this.findLastByUserid(userid);
		
		// Check if productId is valid and get the index of the product in cart
		if(product.getProductId() != null) {
			int index = IntStream.range(0, cart.getProducts().size())
	        .filter(ix -> product.getProductId().equals(cart.getProducts().get(ix).getProductId()))
	        .findFirst().orElse(-1);
			// If the product was not found in cart, add it. Else, only change the amount.
			if(index < 0) {
				cart.getProducts().add(product);
			} else {
				int newAmount = cart.getProducts().get(index).getAmount() + product.getAmount();
				// The amount is limited to 100
				if(newAmount > 100) {
					newAmount = 100;
				}
				
				cart.getProducts().get(index).setAmount(newAmount);
			}
			// Calculate the new total price and return the saved cart
			calculatePrice(cart);
			return repository.save(cart);		
		}
		return null;
	}

	@Override
	public CartEntity removeFromCart(ProductItemEntity product, String userid) throws NotFoundException {
		// Get current cart for this user
		CartEntity cart = this.findLastByUserid(userid);
		// Check if product id is valid and get the index of the product in the cart
		if(product.getProductId() != null) {			
			int index = IntStream.range(0, cart.getProducts().size())
	        .filter(ix -> product.getProductId().equals(cart.getProducts().get(ix).getProductId()))
	        .findFirst().orElse(-1);
			// If the index was not found, there is nothing to remove and a not found exception is thrown
			if(index < 0) {
				throw new NotFoundException("Product with id not found in cart");
			} else {
				// The new amount is calculated and if it's <0 the item is removed
				int newAmount = cart.getProducts().get(index).getAmount() - product.getAmount();
				
				if(newAmount <= 0) {
					cart.getProducts().remove(index);
				} else {
					cart.getProducts().get(index).setAmount(newAmount);
					cart.getProducts().get(index).setPrice(product.getPrice());
				}
			}
			// The price is calculated and the saved cart is returned
			calculatePrice(cart);
			return repository.save(cart);		
		} 

		throw new NotFoundException("Cart was not found");
	}
	/**
	 * Calculates price by adding up the product prices multiplied by the amount of that product
	 * Sets total price on the cart entity
	 * @param cart - the cart entity
	 */
	private void calculatePrice(CartEntity cart) {
		DoubleAdder a = new DoubleAdder();
		cart.getProducts().parallelStream().forEach(product -> a.add(product.getPrice()));
		cart.setTotalPrice(a.doubleValue());
	}

	@Override
	public CartEntity findLastByUserid(String userid) {
		Optional<CartEntity> cartOptional = repository.findByUserid(userid);
		if(!cartOptional.isPresent()) {
			return this.createCart(userid);
		} else {
			return cartOptional.get();
		}
	}

	public CartEntity changeAmount(ProductItemEntity product, String userid, int amount) {
		// Fetches the current cart of the user
		CartEntity cart = this.findLastByUserid(userid);
		// Checks if productid is there and searches for the index of the product in the cart
		if(product.getProductId() != null) {
			int index = IntStream.range(0, cart.getProducts().size())
	        .filter(ix -> product.getProductId().equals(cart.getProducts().get(ix).getProductId()))
	        .findFirst().orElse(-1);
			// If the product is not in the cart, it is added
			if(index < 0) {
				cart.getProducts().add(product);
			} else {
				// If the amount is 0 the product is removed, if not then the amount is set
				if(amount == 0) {
					cart.getProducts().remove(index);
				} else {
					cart.getProducts().get(index).setAmount(amount);
				}
				
			}
			// The price is calculated and the saved cart is returned
			calculatePrice(cart);
			return repository.save(cart);		
		} 

		return null;
	}

}
