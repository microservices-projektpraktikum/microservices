package com.microservices.cartservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.microservices.cartservice.entity.CartEntity;

public interface CartRepository extends CrudRepository<CartEntity, String> {
	
	Optional<CartEntity> findByUserid(String userid);
	
	Integer deleteByUserid(String userid);

}
