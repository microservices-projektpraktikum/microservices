package com.microservices.cartservice.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity(name = "Cart")
@Table(name = "cart_table")
public class CartEntity {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	
	@Column(name = "creation_date", columnDefinition = "DATE")
	private LocalDate creationdate;
	
	@Column(name="user_id")
	private String userid;
	
	@Column(name="total_price")
	private double totalPrice;
	
	@ElementCollection
	@CollectionTable(name = "cart_productids", joinColumns = @JoinColumn(name = "cart_id"))
	private List<ProductItemEntity> products = new ArrayList<>();
	
	public CartEntity() {
		this.creationdate = LocalDate.now();
	}
	
	public CartEntity(String userid, List<ProductItemEntity> products) {
		this.creationdate = LocalDate.now();
		this.userid = userid;
		this.products = products;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}
	
	public LocalDate getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(LocalDate creationdate) {
		this.creationdate = creationdate;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<ProductItemEntity> getProducts() {
		return products;
	}

	public void setProducts(List<ProductItemEntity> products) {
		this.products = products;
	}
	
}
