# cart-service

Cart service

To run, call `docker-compose up`.

- cart-service runs on port `3103`.
- cartdb runs on port `5433`.

## Rest calls

To see all rest calls access http://localhost:3103/swagger-ui.html

![restcalls](./restcalls.PNG)

