# Microservice Thesis

POC of an e-commerce shop built with microservices.

To run the application you need to have Docker installed.

To start the application run:

```bash
docker-compose up
```

You can login with a user account with the following credentials:

|Username|Password|
|---|---|
|user1@example.com|password1|

## Services

| Service  | Person(s)       | Language              | DB       |
| -------- | --------------- | --------------------- | -------- |
| GUI      | Diana & Raminta | React with Redux      | None     |
| Cart     | Diana           | Java with Spring Boot | Postgres |
| Order    | Diana           | Java with Spring Boot | Postgres |
| Shipping | Raminta         | Python                | Postgres |
| User     | Raminta         | Rust                  | MongoDB  |
| Auth     | Christoph       | Node.js with Express  | None     |
| Payment  | Christoph       | Node.js with Express  | Postgres |
| Product  | Christoph       | Node.js with Express  | Postgres |

## Flowcharts

### Calls from GUI

![gui_calls](./assets/gui_calls.png)

### Calls between the services

![service_calls](./assets/service_calls.png)

## Credits

- White Shirt photo: Photo from Anomaly [https://unsplash.com/photos/WWesmHEgXDs](https://unsplash.com/photos/WWesmHEgXDs)
- Trousers photo: Photo from bantersnaps [https://unsplash.com/photos/jC7nVH_Sw8k](https://unsplash.com/photos/jC7nVH_Sw8k)
- Jacket photo: Photo from Axel Vazquez [https://unsplash.com/photos/aPR3u4L3FQY](https://unsplash.com/photos/aPR3u4L3FQY)
