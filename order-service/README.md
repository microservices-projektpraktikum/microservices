# order-service

Order service - currently the object only contains userid and productid

Requirements:
- Docker: https://www.docker.com/products

To run, call `docker-compose up`.
- order-service runs on port `3102`.
- orderdb runs on port `5430`.

#Rest calls

To see all rest calls access http://localhost:3102/swagger-ui.html

![restcalls](./restcalls.PNG)