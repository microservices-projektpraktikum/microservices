package com.microservices.orderservice.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.microservices.orderservice.model.CartModel;

@Service
public class CartConsumer {

	private static final Logger log = LoggerFactory.getLogger(CartConsumer.class);
	
	@Value("${get.cart.url}")
	private String getCartURL;
	
	@Value("${delete.cart.url}")
	private String deleteCartURL;

	@Autowired
	private RestTemplate restTemplate;

	public CartModel getCart(String cartid, String token) {
		// Build the URL for fetching the cart
		log.info("Building url... ");
		String url = getCartURL + cartid + "?token=" + token;
		// Call the cart service using the RestTemplate and the url
		try {
			ResponseEntity<CartModel> responseEntity = restTemplate.exchange(
					url, 
				    HttpMethod.GET, 
				    null, 
				    new ParameterizedTypeReference<CartModel>() {});
			log.info("Got cart...", responseEntity.getBody());
			// Return the response body (received cart)
			return responseEntity.getBody();
		} catch(RestClientException e) {
			// Return null if something went wrong
			e.printStackTrace();
			return null;
		}

	}

	public void deleteCart(String cartid, String token) {	
		String url = deleteCartURL + cartid + "?token=" + token;
		ResponseEntity<CartModel> responseEntity = restTemplate.exchange(
				url, 
			    HttpMethod.DELETE, 
			    null, 
			    new ParameterizedTypeReference<CartModel>() {});	
	}
}
