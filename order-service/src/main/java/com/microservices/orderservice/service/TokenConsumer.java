package com.microservices.orderservice.service;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class TokenConsumer {
	
private static final Logger log = LoggerFactory.getLogger(TokenConsumer.class);
	
	@Value("${token.valid.url}")
	private String tokenValidURL;
	
	@Autowired
	private RestTemplate restTemplate;

	public boolean isTokenValid(String token) {		
		log.info("Building url... ");
		log.info(tokenValidURL+token);
		
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			JSONObject json = new JSONObject();
			json.put("token", token);
			HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);
			HttpEntity<String> response = restTemplate.postForEntity(tokenValidURL, entity, String.class, headers);
			
			json = new JSONObject(response.getBody());
			
			String valid = String.valueOf(json.get("valid"));
			Boolean result = Boolean.valueOf(valid);

		    return result;
		    
		} catch(RestClientException | IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
			return false;
		}

	}

}
