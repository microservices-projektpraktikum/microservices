package com.microservices.orderservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.microservices.orderservice.entity.OrderEntity;
import com.microservices.orderservice.entity.PlaceOrderEntity;
import com.microservices.orderservice.model.CartModel;
import com.microservices.orderservice.repository.OrderRepository;

@Service
public class OrderBusiness {
	
	private static final Logger log = LoggerFactory.getLogger(OrderBusiness.class);
	
	@Autowired
	private CartConsumer consumer;
	
	@Autowired
	private OrderRepository repository;
	
	public OrderEntity placeOrder(PlaceOrderEntity placeOrder) {
		String cartid = placeOrder.getCartid();
		String token = placeOrder.getToken();

		// Get cart from cart-service using the id and token
		try {
			CartModel cart = consumer.getCart(cartid, token);
			log.info("Fetched cart");
			
			// Check for existing cart, create and save order
			if(cart != null) {
				log.info("Cart not null");
				OrderEntity order = new OrderEntity();
				order.setUserid(cart.getUserid());
				order.setProducts(cart.getProducts());
				return repository.save(order);	
			}
			// If the cart is null then it does not exist -> throw an exception
			throw new RestClientException("OrderBusiness: Order could not be placed, cart was not found");
		} catch(Exception e) {
			// If the try went wrong then service is down
			throw new RestClientException("OrderBusiness: Order could not be placed", e.getCause());
		}
		
	}

}
