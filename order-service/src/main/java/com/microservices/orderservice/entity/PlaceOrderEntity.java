package com.microservices.orderservice.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class PlaceOrderEntity {
	
	@NotNull
	private String cartid;
	
	@NotNull
	private String token;

    public PlaceOrderEntity() {
		this.cartid = null;
		this.token = null;
	}
	
	public PlaceOrderEntity(String cartid, String token) {
		this.cartid = cartid;
		this.token = token;
	}

	public String getCartid() {
		return cartid;
	}

	public void setCartid(String cartid) {
		this.cartid = cartid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
