package com.microservices.orderservice.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class ProductItemEntity {
	
	@NotNull
	private int amount;
	
	@NotNull
	private String productid;
	
	public ProductItemEntity() {
		this.amount = 0;
		this.productid = "Default product";
	}
	
	public ProductItemEntity(int amount, String productid) {
		this.amount = amount;
		this.productid = productid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getProductId() {
		return productid;
	}

	public void setProductId(String productid) {
		this.productid = productid;
	}
	
}
