package com.microservices.orderservice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.microservices.orderservice.entity.OrderEntity;

public interface OrderRepository extends CrudRepository<OrderEntity, String> {
	
	List<OrderEntity> findByUserid(String userid);

}
