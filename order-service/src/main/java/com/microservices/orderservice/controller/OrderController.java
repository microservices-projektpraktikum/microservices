package com.microservices.orderservice.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.microservices.orderservice.entity.OrderEntity;
import com.microservices.orderservice.entity.ProductItemEntity;
import com.microservices.orderservice.entity.PlaceOrderEntity;
import com.microservices.orderservice.repository.OrderRepository;
import com.microservices.orderservice.service.OrderBusiness;
import com.microservices.orderservice.service.TokenConsumer;

@RestController
@RequestMapping(path = "orderservice")
@CrossOrigin(origins = "*", maxAge = 3600)
public class OrderController {
	
	@Autowired
	private OrderRepository repository;
	
	@Autowired
	private OrderBusiness business;
	
	@Autowired
	private TokenConsumer tokenConsumer;
	/*
	 * Controller methods have a request mapping, params, a token check using the token consumer
	 * and a call to the business logic for functionality. 
	 * They either return the response or an unauthorized error if the token is not valid
	 */
	@GetMapping("/")
	public Iterable<OrderEntity> fetchAll() {
		return repository.findAll();
	}
	
	@PostMapping("/")
	public OrderEntity insert(@RequestBody OrderEntity order,  @RequestParam(name ="token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return repository.save(order);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@PostMapping("/placeOrder")
	public OrderEntity placeOrder(@RequestBody PlaceOrderEntity order){
		if (tokenConsumer.isTokenValid(order.getToken())) {
			return business.placeOrder(order);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@DeleteMapping("/deleteOrder")
	public void deleteOrder(@RequestParam(name ="id") String orderid, @RequestParam(name ="token") String token){
		if (tokenConsumer.isTokenValid(token)) {
			repository.deleteById(orderid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@GetMapping("/byUserId/{userId}")
	public Iterable<OrderEntity> fetchAllByUserid(@PathVariable(name ="userId") String userid, @RequestParam(name ="token") String token) {
		if (tokenConsumer.isTokenValid(token)) {
			return repository.findByUserid(userid);
		} else {
			throw new ResponseStatusException(
			           HttpStatus.UNAUTHORIZED, "Token is invalid", null);
		}
	}
	
	@GetMapping("/insertSamples")
	public Iterable<OrderEntity> insertSamples() {

		for(int i = 0; i < 10; i++) {
			List<ProductItemEntity> products = Arrays.asList(new ProductItemEntity(i, Integer.toString(i+1)), new ProductItemEntity(i, Integer.toString(i+2)));
			OrderEntity order = new OrderEntity(Integer.toString(i), products);
			repository.save(order);
		}
		
		return repository.findAll();
	}
	
}
