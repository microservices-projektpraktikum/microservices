package com.microservices.orderservice.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.microservices.orderservice.entity.ProductItemEntity;

public class CartModel {
	
	private String id;
	private LocalDate creationdate;
	private String userid;
	private String orderid;
	private long totalPrice;
	private List<ProductItemEntity> products = new ArrayList<>();
	
	public CartModel() {}
	
	public CartModel(String userid, List<ProductItemEntity> products) {
		this.userid = userid;
		this.products = products;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}
	
	public LocalDate getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(LocalDate creationdate) {
		this.creationdate = creationdate;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<ProductItemEntity> getProducts() {
		return products;
	}

	public void setProducts(List<ProductItemEntity> products) {
		this.products = products;
	}

}
