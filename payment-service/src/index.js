const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch');
const { Client } = require('pg');

const app = express();
const port = 3200;

const connection = {
  host: 'paymentdb',
  port: 5432,
  database: 'postgres',
  user: 'postgres',
  password: 'supersecretpassword',
  max: 30,
};

app.use(express.json(), cors());

app.get('/', (_, res) => {
  res.send('Payment service is up!');
});

/**
 * getPaymentById
 */

app.get('/getPaymentById', async (req, res) => {
  const userId = await isTokenValid(req.body.token);
  console.log('getPaymentById userid ' + userId + ' and paymentid ' + req.body.id);
  // return 401 if token is invalid
  if (!userId) {
    res.status(401);
    return;
  }
  // call database to get the payments by paymentID & userID
  const db = new Client(connection);
  await db.connect();
  const result = await db.query('SELECT * FROM payment WHERE paymentid = $1 AND user_id = $2', [req.body.id, userId]);

  await db.end();
  const rows = result.rows;
  // return table rows
  res.status(200).send({ rows });
});

/**
 * getPaymentsForUserId
 */

app.get('/getPaymentsForUserId', async (req, res) => {
  const userId = await isTokenValid(req.body.token);
  console.log('getPaymentsForUserId with userId ' + userId);
  // return 401 if token is invalid
  if (!userId) {
    res.status(401);
    return;
  }

  // call DB to get all payments by user_id
  const db = new Client(connection);
  await db.connect();
  const result = await db.query('SELECT * FROM payment WHERE user_id = $1', [userId]);

  await db.end();
  const rows = result.rows;
  // return table rows
  res.status(200).send({ rows });
});

/**
 * handle payment
 */

app.post('/handlePayment', async (req, res) => {
  console.log('call to handlePayment');
  const userId = await isTokenValid(req.body.token);
  // return 401 if token is invalid
  if (!userId) {
    res.status(401);
    return;
  }

  // return error if no ccNumber or amount was send in the request
  if (!req.body.ccNumber) {
    res.status(400).send({ message: 'No ccNumber in JSON body' });
    return;
  }

  if (!req.body.amount) {
    res.status(502).send({ message: 'No amount in JSON body' });
    return;
  }

  console.log('paymentResult is', req.body.paymentResult);
  const db = new Client(connection);
  await db.connect();

  // return according to paymentResult
  switch (req.body.paymentResult) {
    case 'FAIL':
      res.status(502).send({ message: 'Payment failed' });
      break;
    case 'SUCCESS':
    default:
      try {
        // get biggest paymentId from payment table
        const paymentIdResult = await db.query('SELECT MAX(paymentid) FROM payment');
        const rows = paymentIdResult.rows;
        const newPaymentID = rows[0].max + 1;
        // add new payment (with maxId + 1 as paymentId)
        const result = await db.query('INSERT INTO payment(paymentid, user_id, amount, purchaseTime) VALUES ($1, $2, $3, $4)', [newPaymentID, userId, req.body.amount, new Date()]);
        if (!result) {
          // handle db error
          console.log('error while inserting data into the table; userid', userId, 'amount', req.body.amount);
          res.status(502).send({ message: 'an error occurred while trying to insert data into the db' });
        } else {
          // return created paymentId
          res.status(200).send({ id: newPaymentID });
        }
      } catch (e) {
        console.log('an error occurred', e);
        res.status(502).send({ message: 'an error occurred while trying to insert data into the db' });
      }
  }

  await db.end();
});

/**
 * deletePaymentById
 */

app.post('/deletePaymentById', async (req, res) => {
  const userId = await isTokenValid(req.body.token);
  console.log('deletePaymentById userid ' + userId + ' and paymentid ' + req.body.id);
  // return 401 if token is invalid
  if (!userId) {
    res.status(401);
    return;
  }
  const db = new Client(connection);
  await db.connect();
  const result = await db.query('DELETE FROM payment WHERE paymentid = $1 AND user_id = $2', [req.body.id, userId]);

  await db.end();
  const rows = result.rows;
  res.status(200).send({ message: 'success' });
});

/**
 * Util Functions
 */

async function isTokenValid(token) {
  console.log('call to isTokenValid with token ' + token);
  try {
    // call auth service to check if token is valid
    const response = await fetch('http://auth-service:4000/isTokenValid', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ token: token }),
    });

    const body = await response.json();

    // if token is valid, return the user_id
    if (body.valid) {
      console.log('token from user_id ' + body.user_id + ' is valid.');
      return body.user_id;
    }
    // if token is invalid return null
    console.log('token ' + token + ' is not valid');
    return null;
  } catch (e) {
    // handle auth service error
    console.log('error callid auth service', e);
    return null;
  }
}

const server = app.listen(port, () => console.log(`Payment service is listening on port ${port}!`));

module.exports = server;
