DROP TABLE IF EXISTS payment;


CREATE TABLE payment (paymentid SERIAL PRIMARY KEY,
                    user_id SERIAL,
                    amount FLOAT(2),
                    purchaseTime TIMESTAMP);

INSERT INTO payment(paymentid, user_id, amount, purchaseTime)
VALUES (1, 1, 12.99, (TIMESTAMP '2020-03-12 12:20:42'));

INSERT INTO payment(paymentid, user_id, amount, purchaseTime)
VALUES (2, 2, 120.00, (TIMESTAMP '2020-02-24 14:31:38'));

INSERT INTO payment(paymentid, user_id, amount, purchaseTime)
VALUES (3, 1, 3.99, (TIMESTAMP '2020-03-12 06:30:27'));

INSERT INTO payment(paymentid, user_id, amount, purchaseTime)
VALUES (4, 3, 02.17, (TIMESTAMP '2019-11-02 03:11:02'));