# Payment Service

## API

### /getPaymentById

**Expected Request Body:**

```json
{
  "token": "the JWT token",
  "id": "payment id"
}
```

**Possible responses:**

- **200** if payment has the same user_id as the user_id in the token & token is valid

```json
{
  "rows": [
    {
      "paymentid": 2,
      "user_id": 2,
      "amount": 120,
      "purchasetime": "2020-02-24T14:31:38.000Z"
    }
  ]
}
```

- **200** if payment doesn't have the same user_id as the user_id in the token & token is valid

```json
{
  "rows": []
}
```

- **401** if token is invalid

---

### /getPaymentsForUserId

Returns all payments from a userId. UserId is extracted from the token.

**Expected Request Body:**

```json
{
  "token": "the JWT token"
}
```

**Possible responses:**

Same as _getPaymentById_

---

### /handlePayment

**Expected Request Body:**

```json
{
  "token": "the JWT token",
  "ccNumber": "credit card number",
  "amount": "amount",
  "paymentResult": "optional. Values can be 'FAIL' or 'SUCCESS'"
}
```

**Possible responses:**

- **200** if everything works

```json
{
  "id": "paymentid"
}
```

- **400** if ccNumber is missing

```json
{
  "message": "No ccNumber in JSON body"
}
```

- **502** if amount is missing

```json
{
  "message": "No amount in JSON body"
}
```

- **502** if paymentResult is "FAIL"

```json
{
  "message": "Payment failed"
}
```

- **502** if db is not reachable

```json
{
  "message": "an error occurred while trying to insert data into the db"
}
```

### /deletePaymentById

**Expected Request Body:**

```json
{
  "token": "the JWT token",
  "id": "payment id"
}
```

**Possible responses:**

- **200** if entry was deleted

```json
{
  "message": "success"
}
```
