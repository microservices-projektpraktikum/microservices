# Authentication Service

Uses [JWT](https://jwt.io) for authentication.

## API

### /login

Calls the **user-service** to check if credentials are valid.

**Expected Request Body:**

```json
{
  "email": "email of the user",
  "password": "hashed password"
}
```

**Possible responses:**

- **200** if credentials are correct

```json
{
  "token": "the JWT token"
}
```

- **401** if credentials are invalid

```json
{
  "message": "Unauthorized"
}
```

- **500** (when user_service not available)

```json
{
  "message": "An error occured while trying to contact the user_service"
}
```

---

### /logout

Calls the user-service to set the logoutTime to the current time

**Expected Request Body:**

```json
{
  "token": "the JWT token"
}
```

**Possible responses:**

- **200** If logout was successful

```json
{
  "message": "Logout successful"
}
```

- **500** when user_service not available

```json
{
  "message": "An error occured while trying to contact the user_service"
}
```

- **500** when other error occurred

```json
{
  "message": "An error occurred"
}
```

---

### /isTokenValid

Checks if token is valid. Calls user-service to compare logoutTime with issued at field of the token.

**Expected Request Body:**

```json
{
  "token": "the JWT token"
}
```

**Possible responses:**

- **200** if token is valid

```json
{
  "user_id": "the user_id in the token",
  "valid": true
}
```

- **200** if token is invalid

```json
{
  "valid": false
}
```

- **500** when user_service not available

```json
{
  "message": "An error occured while trying to contact the user_service"
}
```
