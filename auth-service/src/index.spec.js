const request = require('supertest');
const app = require('./index');
require('jest-fetch-mock').enableMocks();

/* Test if server listens */

describe('AuthService', () => {
  afterAll((done) => {
    app.close(done);
  });

  it('should check if service is up', async () => {
    const res = await request(app).get('/');
    expect(res.status).toBe(200);
    expect(res.text).toContain('Auth service is up');
  });
});

/* Login Tests */

describe('login', () => {
  afterAll((done) => {
    app.close(done);
  });

  it('should login successfully', async () => {
    fetch.mockResponse(
      JSON.stringify({
        user_id: 11,
      })
    );

    const res = await request(app).post('/login').send({
      email: 'mr@mrx.com',
      password: 'secretpw',
    });
    expect(res.status).toBe(200);
    expect(res.body.token).toBeDefined();
  });

  it('should fail login', async () => {
    fetch.mockResponse(
      JSON.stringify({
        body: 'Invalid user name or password',
      }),
      { status: 401 }
    );

    const res = await request(app).post('/login').send({
      email: 'mr@mrx.com',
      password: 'secretpw',
    });

    expect(res.status).toBe(401);
    expect(res.body.message).toContain('Unauthorized');
  });
});

/* logout Tests */

describe('logout', () => {
  afterAll((done) => {
    app.close(done);
  });

  it('should logout successfully', async () => {
    fetch.mockResponse(
      JSON.stringify({
        body: 'User logged out',
      }),
      { status: 200 }
    );

    const res = await request(app).post('/logout').send({
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(200);
    expect(res.body.message).toContain('Logout successful');
  });

  it('should logout successfully', async () => {
    fetch.mockResponse(
      JSON.stringify({
        body: 'Failed logout',
      }),
      { status: 500 }
    );

    const res = await request(app).post('/logout').send({
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(500);
    expect(res.body.message).toContain('An error occurred');
  });
});

/* Is Token Valid Tests */

describe('isTokenValid', () => {
  afterAll((done) => {
    app.close(done);
  });

  it('should validate if no last_logout was found', async () => {
    fetchMock.mockOnce(
      JSON.stringify({
        last_logout: null,
      }),
      { status: 200 }
    );
    const res = await request(app).post('/isTokenValid').send({
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(200);
    expect(res.body.valid).toBe(true);
  });

  it('should validate if new token was issued after last_logut', async () => {
    fetchMock.mockOnce(
      JSON.stringify({
        last_logout: 1000000001,
      }),
      { status: 200 }
    );
    const res = await request(app).post('/isTokenValid').send({
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(200);
    expect(res.body.valid).toBe(true);
  });

  it('should invalidate if new token was issued before last_logut', async () => {
    fetchMock.mockOnce(
      JSON.stringify({
        last_logout: 2000000001,
      }),
      { status: 200 }
    );
    const res = await request(app).post('/isTokenValid').send({
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(200);
    expect(res.body.valid).toBe(false);
  });

  it('should invalidate token is invalid', async () => {
    fetchMock.mockOnce(
      JSON.stringify({
        last_logout: 2000000001,
      }),
      { status: 200 }
    );
    const res = await request(app).post('/isTokenValid').send({
      token: 'deJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yQG1yeC5jb20iLCJ1c2VyX2lkIjoxMSwiaWF0IjoxNTkxOTYwMjk3fQ.pMqRfGZKqdHVIBoh50iwWIndgFePPhBTAwo4JbrrKGk',
    });

    expect(res.status).toBe(200);
    expect(res.body.valid).toBe(false);
  });
});
