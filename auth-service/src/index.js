require('dotenv').config();
const express = require('express');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const fetch = require('node-fetch');

const app = express();
const port = 4000;

app.use(express.json(), cors());

/**
 * startup
 */

app.get('/', (_, res) => {
  res.send('Auth service is up!');
});

const server = app.listen(port, () => console.log(`Auth service is listening on port ${port}!`));

/**
 * login
 */

app.post('/login', async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  console.log('login request from ' + email);

  try {
    const postBody = { email: email, password: password };

    // call user service to validate credentials
    const response = await fetch('http://user-service:6002/validate', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(postBody),
    });

    const status = response.status;
    const body = await response.json();

    if (status >= 200 && status < 300) {
      const jwtObj = {
        email: email,
        user_id: body.user_id,
      };
      // create JWT token from email and user_id
      const access_token = jwt.sign(jwtObj, process.env.ACCESS_TOKEN_SECRET);

      console.log('login for ' + email + ' with user_id ' + jwtObj.user_id + ' successful');

      // return JWT token in response
      res.status(200).json({ token: access_token });
      return;
    }
  } catch (error) {
    // handle user service error
    console.error(error);
    res.status(500).json({
      message: 'An error occured while trying to contact the user_service',
    });
    return;
  }

  console.log('login for ' + email + ' not successful');

  res.status(401).json({ message: 'Unauthorized' });
});

/**
 * logout
 */

app.post('/logout', async (req, res) => {
  console.log('call to logout');
  const token = req.body.token;

  // decoded JWT token
  let decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);

  try {
    console.log('call to sign_out');

    // call user service sign_out
    const response = await fetch(`http://user-service:6002/sign_out/${decoded.user_id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const status = response.status;

    console.log('user-service responded with ' + status);

    if (status >= 200 && status < 300) {
      console.log('logged out user with id: ' + decoded.user_id);
      res.status(200).json({ message: 'Logout successful' });
      return;
    }
  } catch (e) {
    // handle user service error
    console.error(e);
    res.status(500).json({
      message: 'An error occured while trying to contact the user_service',
    });
  }

  res.status(500).json({ message: 'An error occurred' });
});

/**
 * isTokenValid
 */

app.post('/isTokenValid', async (req, res) => {
  console.log('call to isTokenValid');
  const token = req.body.token;
  // issued at timestamp
  let iat;
  let user_id;
  // the decoded JWT token
  let decoded;

  console.log('verify token ' + token);

  try {
    // verify JWT token
    decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  } catch (e) {
    // invalid token
    console.log('error with token', e);
    res.status(200).json({ valid: false });
    return;
  }

  iat = decoded.iat;
  user_id = decoded.user_id;
  console.log('token from user_id ' + decoded.user_id + ' is verified');

  try {
    // get last user logout from user service
    const response = await fetch(`http://user-service:6002/last_logout/${user_id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const body = await response.json();

    const logoutTime = body.last_logout;

    // if logout happened after the token was issued, then the token is invalid
    if (logoutTime && logoutTime > iat) {
      console.log('Token from user_id ' + user_id + ' is invalid because logout happened before');
      console.log('iat from token = ' + iat + ' ; logoutTime = ' + logoutTime);
      res.status(200).json({ valid: false });
      return;
    }
  } catch (e) {
    // handle user service error
    console.error(e);
    res.status(500).json({
      message: 'An error occured while trying to contact the user_service',
    });
    return;
  }

  // token is valid
  console.log('Token from user_id ' + user_id + ' is valid');
  res.status(200).json({
    user_id: user_id,
    valid: true,
  });
});

module.exports = server;
