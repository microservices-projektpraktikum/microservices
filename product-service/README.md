# Product Service

## API

### /getProducts

Returns all products in the database.

**Possible responses:**

- **200** if db call was successful

```json
{
  "rows": [
    "0": {
      "id": 1,
      "name": "Super T-Shirt",
      "description": "Ein super T-Shirt zum anziehen",
      "price": 3.99,
      "image": {
        "type": "Buffer",
        "data": "buffer data of the image"
      }
    },
    "...":"..."
  ]
}
```

- **500** if db was not reachable / other error

```json
{
  "message": "An error occured"
}
```

---

### /getProductById

Returns the product with the specified ID from the database.

**Expected Request Body:**

```json
{
  "id": "the id of the product"
}
```

**Possible responses:**

- **200** if db call was successful

```json
{
  "rows": [
    "0": {
      "id": 1,
      "name": "Super T-Shirt",
      "description": "Ein super T-Shirt zum anziehen",
      "price": 3.99,
      "image": {
        "type": "Buffer",
        "data": "buffer data of the image"
      }
    }
  ]
}
```

- **500** if db was not reachable / other error

```json
{
  "message": "An error occured"
}
```
