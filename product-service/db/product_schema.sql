DROP TABLE IF EXISTS product;


CREATE TABLE product (id SERIAL PRIMARY KEY,
                     name VARCHAR (40) NOT NULL,
                     description VARCHAR (200),
                     price FLOAT (2) NOT NULL,
                     image BYTEA);


INSERT INTO product (id, name, description, price, image)
VALUES (1,
        'Super T-Shirt',
        'Ein super T-Shirt zum anziehen',
        3.99,
        pg_read_binary_file('/docker-entrypoint-initdb.d/pictures/shirt.jpg'));


INSERT INTO product (id, name, description, price, image)
VALUES (2,
        'Tolle Hose',
        'Eine tolle Hose!',
        123.32,
        pg_read_binary_file('/docker-entrypoint-initdb.d/pictures/trousers.jpg'));


INSERT INTO product (id, name, description, price, image)
VALUES (3,
        'Einfache Jacke',
        'Eine einfache Jacke. Mehr nicht.',
        10.0,
        pg_read_binary_file('/docker-entrypoint-initdb.d/pictures/mantle.jpg'));
