const express = require('express');
const { Client } = require('pg');
const cors = require('cors');

const app = express();
const port = 3100;

app.use(express.json(), cors());

const connection = {
  host: 'productdb',
  port: 5432,
  database: 'postgres',
  user: 'postgres',
  password: 'supersecretpassword',
  max: 30,
};

app.get('/', (_, res) => {
  res.send('Product service is up!');
});

/***
 * getProducts
 */

app.get('/getProducts', async (_, res) => {
  console.log('call to /getProducts');
  try {
    // create connection to DB and select all products
    const db = new Client(connection);
    await db.connect();
    const result = await db.query('SELECT * FROM product');

    await db.end();
    const rows = result.rows;
    // return table rows
    res.status(200).send({ rows });
  } catch (e) {
    // handle DB error
    console.log('An error occured', e);
    res.status(500).send({ message: 'An error occurred' });
  }
});

/***
 * getProductById
 */

app.get('/getProductById', async (req, res) => {
  const id = req.query.id || req.body.id;
  // return error if no id sent in the request
  if (!id) {
    console.log('no id as query param or in body found!');
    res.status(500).send({ message: 'No id found' });
    return;
  }
  console.log('getProduct by id', id);
  try {
    // create connection to DB and select the product with specified id
    const db = new Client(connection);
    await db.connect();
    const result = await db.query('SELECT * FROM product WHERE id = $1', [id]);

    await db.end();
    const rows = result.rows;
    // return table rows
    res.status(200).send({ rows });
  } catch (e) {
    // handle DB error
    console.log('An error occured', e);
    res.status(500).send({ message: 'An Error occurred' });
  }
});

const server = app.listen(port, () => console.log(`product service is listening on port ${port}!`));

module.exports = server;
