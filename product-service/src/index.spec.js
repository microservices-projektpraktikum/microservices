const request = require('supertest');
const app = require('./index');
const { Client } = require('pg');

jest.mock('pg', () => {
  const mockClient = {
    connect: jest.fn(),
    query: jest.fn(),
    end: jest.fn(),
  };
  return { Client: jest.fn(() => mockClient) };
});

describe('Product service', () => {
  afterAll((done) => {
    app.close(done);
  });

  it('should check if service is up', async () => {
    const res = await request(app).get('/');
    expect(res.status).toBe(200);
    expect(res.text).toContain('Product service is up!');
  });
});

describe('Product service', () => {
  let client;
  beforeEach(() => {
    client = new Client();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll((done) => {
    app.close(done);
  });

  it('should get Products', async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    const res = await request(app).get('/getProducts');
    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.body.rows).toEqual([]);
  });

  it('should get Product by Id with query param', async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    const res = await request(app).get('/getProductById?id=1');
    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.body.rows).toEqual([]);
  });

  it('should get Product by Id in body', async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    const res = await request(app).get('/getProductById').send({ id: 1 });
    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.body.rows).toEqual([]);
  });

  it('should fail when no id is present in query param or body', async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
    const res = await request(app).get('/getProductById');
    expect(client.connect).toBeCalledTimes(0);
    expect(client.end).toBeCalledTimes(0);
    expect(res.status).toBe(500);
  });
});
