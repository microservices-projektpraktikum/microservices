DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS addresses;

CREATE TABLE users(
   user_id SERIAL PRIMARY KEY,
   email VARCHAR (320) UNIQUE NOT NULL,
   firstname VARCHAR (50),
   lastname VARCHAR (50),
   password text NOT NULL,
   created_on timestamptz NOT NULL,
   last_logout timestamptz
);

CREATE TABLE addresses(
   address_id SERIAL PRIMARY KEY,
   user_id SERIAL REFERENCES users ON DELETE CASCADE,
   fullname VARCHAR (100),
   street VARCHAR (100),
   city VARCHAR (100),
   postcode VARCHAR (100),
   country VARCHAR (100),
   phone VARCHAR (100)
   
);



-- Fill in the 'users' table with 10 predefined users
-- Passwords are iterated for each user: password1, password2, etc. Already hashed with sha256
INSERT INTO users (email, firstname, lastname, password, created_on, last_logout) VALUES (
   'user1@example.com', 'FirstName1', 'LastName1', '0b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e', now(), now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user2@example.com', 'FirstName2', 'LastName2', '6cf615d5bcaac778352a8f1f3360d23f02f34ec182e259897fd6ce485d7870d4', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user3@example.com', 'FirstName3', 'LastName3', '5906ac361a137e2d286465cd6588ebb5ac3f5ae955001100bc41577c3d751764', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user4@example.com', 'FirstName4', 'LastName4', 'b97873a40f73abedd8d685a7cd5e5f85e4a9cfb83eac26886640a0813850122b', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user5@example.com', 'FirstName5', 'LastName5', '8b2c86ea9cf2ea4eb517fd1e06b74f399e7fec0fef92e3b482a6cf2e2b092023', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user6@example.com', 'FirstName6', 'LastName6', '598a1a400c1dfdf36974e69d7e1bc98593f2e15015eed8e9b7e47a83b31693d5', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user7@example.com', 'FirstName7', 'LastName7', '5860836e8f13fc9837539a597d4086bfc0299e54ad92148d54538b5c3feefb7c', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user8@example.com', 'FirstName8', 'LastName8', '57f3ebab63f156fd8f776ba645a55d96360a15eeffc8b0e4afe4c05fa88219aa', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user9@example.com', 'FirstName9', 'LastName9', '9323dd6786ebcbf3ac87357cc78ba1abfda6cf5e55cd01097b90d4a286cac90e', now());
INSERT INTO users (email, firstname, lastname, password, created_on) VALUES (
   'user10@example.com', 'FirstName10', 'LastName10', 'aa4a9ea03fcac15b5fc63c949ac34e7b0fd17906716ac3b8e58c599cdc5a52f0', now());
INSERT INTO addresses (user_id, fullname, street, city, postcode, country, phone) VALUES (
   3, 'Alice Liddell', 'Baker 100/1', 'Clover', '1234', 'Wonderland', '123456789');