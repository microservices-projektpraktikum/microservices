mod service;
use crate::service::{setup_app_config, setup_postgres_config};
use actix_cors::Cors;
use actix_web::{middleware::Logger, App, HttpServer};

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=trace");
    env_logger::init();
    println!("Starting user service: 0.0.0.0:6002");
    // Setup and start the server
    HttpServer::new(move || {
        App::new()
            .wrap(Cors::default())
            .wrap(Logger::default())
            .data(setup_postgres_config())
            .configure(setup_app_config)
    })
    .bind("0.0.0.0:6002")?
    .run()
    .await
}
