use actix_cors::Cors;
use actix_web::{web, HttpResponse};
use chrono::{DateTime, Utc};
use deadpool_postgres::{Manager, Pool};
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;
use tokio_postgres::{Config, Row};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserId {
    user_id: i32,
}
impl From<Row> for UserId {
    fn from(row: Row) -> Self {
        Self {
            user_id: row.get("user_id"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Firstname {
    firstname: Option<String>,
}
impl From<Row> for Firstname {
    fn from(row: Row) -> Self {
        Self {
            firstname: row.get("firstname"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    pub email: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ChangePassword {
    pub token: String,
    pub old_password: String,
    pub new_password: String,
}

#[derive(Deserialize, Debug)]
pub struct LastLogout {
    last_logout: Option<DateTime<Utc>>,
}
impl From<Row> for LastLogout {
    fn from(row: Row) -> Self {
        Self {
            last_logout: row.get("last_logout"),
        }
    }
}

#[derive(Serialize, Debug)]
pub struct LastLogoutTimeStamp {
    last_logout: Option<i64>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Profile {
    email: String,
    firstname: Option<String>,
    lastname: Option<String>,
    created_on: DateTime<Utc>,
}
impl From<Row> for Profile {
    fn from(row: Row) -> Self {
        Self {
            email: row.get("email"),
            firstname: row.get("firstname"),
            lastname: row.get("lastname"),
            created_on: row.get("created_on"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SetProfile {
    pub token: String,
    pub firstname: Option<String>,
    pub lastname: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Address {
    fullname: String,
    street: String,
    city: String,
    postcode: String,
    country: String,
    phone: String,
}
impl From<Row> for Address {
    fn from(row: Row) -> Self {
        Self {
            fullname: row.get("fullname"),
            street: row.get("street"),
            city: row.get("city"),
            postcode: row.get("postcode"),
            country: row.get("country"),
            phone: row.get("phone"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SetAddress {
    pub token: String,
    pub fullname: String,
    pub street: String,
    pub city: String,
    pub postcode: String,
    pub country: String,
    pub phone: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct IsTokenValid {
    user_id: Option<i32>,
    valid: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Token {
    pub token: String,
}

/// Call Auth-service and check if the token is valid
async fn is_token_valid(user_id: i32, token: &str) -> Result<(), HttpResponse> {
    // Prepare connection to the auth-service
    let auth_client = reqwest::Client::new();
    let mut map = HashMap::new();
    map.insert("token", token);

    // Call auth-service
    match auth_client
        .post(&String::from("http://auth-service:4000/isTokenValid"))
        .json(&map)
        .send()
        .await
    {
        Ok(resp) => match resp.status() {
            reqwest::StatusCode::OK => {
                let is_token_valid: IsTokenValid = match resp.json().await {
                    Ok(t) => t,
                    Err(err) => {
                        return Err(HttpResponse::InternalServerError().json(format!(
                            "Could not validate token through auth-service: {}",
                            err
                        )))
                    }
                };
                if !is_token_valid.valid {
                    return Err(HttpResponse::Unauthorized().json("Invalid token."));
                }
                match is_token_valid.user_id {
                    Some(id) => {
                        if id != user_id {
                            return Err(
                                HttpResponse::Unauthorized().json("Invalid token or user id.")
                            );
                        }
                    }
                    None => {
                        return Err(HttpResponse::Unauthorized().json("Invalid token or user id."))
                    }
                }
                Ok(())
            }
            // Accepted, but invalid or something else was wrong
            err => Err(HttpResponse::InternalServerError().json(format!(
                "Could not validate token through auth-service: {}",
                err
            ))),
        },
        // Could not connect or not accepted
        Err(err) => Err(HttpResponse::InternalServerError().json(format!(
            "Could not validate token through auth-service: {}",
            err
        ))),
    }
}

/// Call Cart-service and request to delete a cart for the user
async fn delete_cart(user_id: i32, token: &str) -> Result<(), HttpResponse> {
    // Prepare connection to the cart-service
    let cart_client = reqwest::Client::new();
    let mut map = HashMap::new();
    map.insert("token", token);

    // Call cart-service
    match cart_client
        .delete(&format!(
            "http://cart-service:3103/cartservice/deletecartbyuser/{}?token={}",
            &user_id,
            &token
        ))
        .json(&map)
        .send()
        .await
    {
        Ok(resp) => match resp.status() {
            reqwest::StatusCode::OK => Ok(()),
            // Accepted, but something else was wrong
            err => Err(HttpResponse::InternalServerError().json(format!(
                "Could not delete user due to cart-service: {}",
                err
            ))),
        },
        // Could not connect or not accepted
        Err(err) => Err(HttpResponse::InternalServerError().json(format!(
            "Could not delete user due to cart-service: {}",
            err
        ))),
    }
}

/// Add new user to the DB
pub async fn register(user: web::Json<User>, data: web::Data<Pool>) -> HttpResponse {
    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Check if the user does not exist already
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE email=lower($1))")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.email]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if exists {
        return HttpResponse::Conflict()
            .json(format!("User with email '{}' already exists", user.email));
    }

    // Add user to the DB
    let instert_statement = match client.prepare("INSERT INTO users (email, password, created_on) VALUES (lower($1), $2, now()) RETURNING user_id").await{
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e))
    };
    let new_id: i32 = match client
        .query(&instert_statement, &[&user.email, &user.password])
        .await
    {
        Ok(id) => id,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }[0]
    .get(0);
    HttpResponse::Ok().json(UserId { user_id: new_id })
}

/// Validate email and password
pub async fn validate(user: web::Json<User>, data: web::Data<Pool>) -> HttpResponse {
    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows where email and password match from the DB
    let statement = match client
        .prepare("SELECT user_id FROM users WHERE email=lower($1) AND password=$2")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows: Vec<UserId> = match client
        .query(&statement, &[&user.email, &user.password])
        .await
    {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }
    .into_iter()
    .map(UserId::from)
    .collect();

    // Return response according to the amount of rows found
    match rows.len() {
        0 => HttpResponse::Unauthorized().json("Invalid user name or password"),
        1 => HttpResponse::Ok().json(rows[0].clone()),
        _ => HttpResponse::InternalServerError().json(format!(
            "Database is broken. Found {} entries with email '{}'",
            rows.len(),
            user.email
        )),
    }
}

/// Change user password
pub async fn change_password(
    user: web::Path<UserId>,
    payload: web::Json<ChangePassword>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Prepare DB statement
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE user_id=$1 AND password=$2)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };

    // Check if the old password was correct
    let rows = match client
        .query(&statement, &[&user.user_id, &payload.old_password])
        .await
    {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if !exists {
        return HttpResponse::Unauthorized().json("Invalid user id or password");
    }

    // Prepare update statement
    let update_statement = match client
        .prepare("UPDATE users SET password=$1 WHERE user_id=$2")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };

    // Update password
    if let Err(e) = client
        .query(&update_statement, &[&payload.new_password, &user.user_id])
        .await
    {
        return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e));
    };
    HttpResponse::Ok().json(format!("Password for '{}' was changed", &user.user_id))
}

/// Sign out
pub async fn sign_out(user: web::Path<UserId>, data: web::Data<Pool>) -> HttpResponse {
    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows where user id match from the DB
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE user_id=$1)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if !exists {
        return HttpResponse::Conflict().json("Invalid user id");
    }

    // Update last_logout in the DB
    let update_statement = match client
        .prepare("UPDATE users SET last_logout=now() WHERE user_id=$1")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    if let Err(e) = client.query(&update_statement, &[&user.user_id]).await {
        return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e));
    }
    HttpResponse::Ok().json(format!("User {} was logged out", &user.user_id))
}

/// Delete user
pub async fn delete(
    user: web::Path<UserId>,
    payload: web::Json<Token>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows where user id match from the DB
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE user_id=$1)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if !exists {
        return HttpResponse::Conflict().json("Invalid user id");
    }

    // Inform cart-service about user deletion
    if let Err(e) = delete_cart(user.user_id, &payload.token).await {
        return e;
    }

    // Execute user deletion in local DB
    let update_statement = match client.prepare("DELETE FROM users WHERE user_id=$1").await {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    if let Err(e) = client.query(&update_statement, &[&user.user_id]).await {
        return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e));
    }
    HttpResponse::Ok().json(format!("User {} was deleted", &user.user_id))
}

/// Send last log out time stamp
pub async fn last_logout(user: web::Path<UserId>, data: web::Data<Pool>) -> HttpResponse {
    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows with last_logout where user id match from the DB
    let statement = match client
        .prepare("SELECT last_logout FROM users WHERE user_id=$1")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows: Vec<LastLogout> = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }
    .into_iter()
    .map(LastLogout::from)
    .collect();

    // Return response according to the number of rows found
    match rows.len() {
        0 => HttpResponse::Conflict().json("Invalid user id"),
        1 => HttpResponse::Ok().json(LastLogoutTimeStamp {
            last_logout: rows[0].last_logout.map(|t| t.timestamp()),
        }),
        _ => HttpResponse::InternalServerError().json(format!(
            "Database is broken. Found {} entries with user id '{}'",
            rows.len(),
            user.user_id
        )),
    }
}

/// Send user profile data (created_on, email, first name, last name)
pub async fn get_profile(
    user: web::Path<UserId>,
    payload: web::Json<Token>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows user profile where user id match from the DB
    let statement = match client
        .prepare("SELECT email, firstname, lastname, created_on FROM users WHERE user_id=$1")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows: Vec<Profile> = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }
    .into_iter()
    .map(Profile::from)
    .collect();

    // Return response according to the number of rows found
    match rows.len() {
        0 => HttpResponse::Conflict().json("Invalid user id"),
        1 => HttpResponse::Ok().json(rows[0].clone()),
        _ => HttpResponse::InternalServerError().json(format!(
            "Database is broken. Found {} entries with user id '{}'",
            rows.len(),
            user.user_id
        )),
    }
}

/// Set user profile data (first name, last name)
pub async fn set_profile(
    user: web::Path<UserId>,
    payload: web::Json<SetProfile>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows where user id match from the DB
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE user_id=$1)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if !exists {
        return HttpResponse::Conflict().json("Invalid user id");
    }

    // Update user's first and last names in the DB
    let update_statement = match client
        .prepare("UPDATE users SET firstname=$2, lastname=$3 WHERE user_id=$1")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    if let Err(e) = client
        .query(
            &update_statement,
            &[&user.user_id, &payload.firstname, &payload.lastname],
        )
        .await
    {
        return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e));
    }
    HttpResponse::Ok().json(format!(
        "User's {} first and last names were changed.",
        &user.user_id
    ))
}

/// Send user first name
pub async fn get_firstname(
    user: web::Path<UserId>,
    payload: web::Json<Token>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows with firstname where user id match from the DB
    let statement = match client
        .prepare("SELECT firstname FROM users WHERE user_id=$1")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows: Vec<Firstname> = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }
    .into_iter()
    .map(Firstname::from)
    .collect();

    // Return response according to the number of rows found
    match rows.len() {
        0 => HttpResponse::Conflict().json("Invalid user id"),
        1 => HttpResponse::Ok().json(rows[0].clone()),
        _ => HttpResponse::InternalServerError().json(format!(
            "Database is broken. Found {} entries with user id '{}'",
            rows.len(),
            user.user_id
        )),
    }
}

/// Send user address
pub async fn get_address(
    user: web::Path<UserId>,
    payload: web::Json<Token>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows with user address where user id matches
    let statement = match client.prepare("SELECT user_id, fullname, street, city, postcode, country, phone FROM addresses WHERE user_id=$1").await {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e))
    };
    let rows: Vec<Address> = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    }
    .into_iter()
    .map(Address::from)
    .collect();

    // Return response according to the number of rows found
    match rows.len() {
        0 => HttpResponse::Ok().json(()),
        1 => HttpResponse::Ok().json(rows[0].clone()),
        _ => HttpResponse::InternalServerError().json(format!(
            "Database is broken. Found {} entries with user id '{}'",
            rows.len(),
            user.user_id
        )),
    }
}

/// Set user address
pub async fn set_address(
    user: web::Path<UserId>,
    payload: web::Json<SetAddress>,
    data: web::Data<Pool>,
) -> HttpResponse {
    // First check if the token is valid
    if let Err(e) = is_token_valid(user.user_id, &payload.token).await {
        return e;
    }

    // Prepare connection to the DB
    let client = match data.get().await {
        Ok(c) => c,
        Err(_e) => {
            return HttpResponse::InternalServerError().json("Could not connect to the User DB")
        }
    };

    // Get rows where user id match from the DB
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM users WHERE user_id=$1)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    if !exists {
        return HttpResponse::Conflict().json("Invalid user id");
    }

    // Check if address exists
    let statement = match client
        .prepare("SELECT exists(SELECT 1 FROM addresses WHERE user_id=$1)")
        .await
    {
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let rows = match client.query(&statement, &[&user.user_id]).await {
        Ok(r) => r,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e)),
    };
    let exists: bool = rows[0].get(0);
    let mut update_statement = match client.prepare(
        "INSERT INTO addresses (user_id, fullname, street, city, postcode, country, phone) 
        VALUES ($1, $2, $3, $4, $5, $6, $7)").await{
        Ok(s) => s,
        Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e))
    };
    if exists {
        update_statement = match client.prepare(
            "UPDATE addresses SET fullname=$2, street=$3, city=$4, postcode=$5, country=$6, phone=$7 WHERE user_id=$1").await{
            Ok(s) => s,
            Err(e) => return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e))
        };
    }
    if let Err(e) = client
        .query(
            &update_statement,
            &[
                &user.user_id,
                &payload.fullname,
                &payload.street,
                &payload.city,
                &payload.postcode,
                &payload.country,
                &payload.phone,
            ],
        )
        .await
    {
        return HttpResponse::InternalServerError().json(format!("Error User DB: {}", e));
    }
    HttpResponse::Ok().json(format!("User's {} address was changed.", &user.user_id))
}

/// Server greeting
pub async fn hello() -> HttpResponse {
    HttpResponse::Ok().body("User service is up!")
}

/// Setup DB configuration
pub fn setup_postgres_config() -> Pool {
    let mut cfg = Config::new();
    cfg.host("userdb");
    cfg.port(5432);
    cfg.user("admin");
    cfg.password("password");
    cfg.dbname("users");
    Pool::new(Manager::new(cfg, tokio_postgres::NoTls), 15)
}

/// Setup server configurations
pub fn setup_app_config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("/register").route(web::post().to(register)));
    cfg.service(
        web::resource("/validate")
            .wrap(
                Cors::new()
                    .allowed_origin("http://auth-service:4000")
                    .allowed_methods(vec!["POST"])
                    .finish(),
            )
            .route(web::post().to(validate)),
    );
    cfg.service(
        web::resource("/sign_out/{user_id}")
            .wrap(
                Cors::new()
                    .allowed_origin("http://auth-service:4000")
                    .allowed_methods(vec!["POST"])
                    .finish(),
            )
            .route(web::post().to(sign_out)),
    );
    cfg.service(
        web::resource("/last_logout/{user_id}")
            .wrap(
                Cors::new()
                    .allowed_origin("http://auth-service:4000")
                    .allowed_methods(vec!["POST"])
                    .finish(),
            )
            .route(web::post().to(last_logout)),
    );
    cfg.service(web::resource("/change_password/{user_id}").route(web::post().to(change_password)));
    cfg.service(web::resource("/delete/{user_id}").route(web::delete().to(delete)));
    cfg.service(web::resource("/get_profile/{user_id}").route(web::post().to(get_profile)));
    cfg.service(web::resource("/set_profile/{user_id}").route(web::post().to(set_profile)));
    cfg.service(web::resource("/get_address/{user_id}").route(web::post().to(get_address)));
    cfg.service(web::resource("/set_address/{user_id}").route(web::post().to(set_address)));
    cfg.service(web::resource("/firstname/{user_id}").route(web::post().to(get_firstname)));
    cfg.service(web::resource("/").route(web::get().to(hello)));
}
