extern crate user_service;
use actix_cors::Cors;
use actix_rt;
use actix_web::body::{Body, ResponseBody};
use actix_web::dev::{Service, ServiceResponse};
use actix_web::http::StatusCode;
use actix_web::test::{self, TestRequest};
use actix_web::App;
use user_service::service::{
    setup_app_config, setup_postgres_config, ChangePassword, SetAddress, SetProfile, Token, User,
};

trait BodyTest {
    fn as_str(&self) -> &str;
}

/// Implement basic response body conversion. Will panic on empty body.
impl BodyTest for ResponseBody<Body> {
    fn as_str(&self) -> &str {
        match self {
            ResponseBody::Body(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
            ResponseBody::Other(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
        }
    }
}

#[actix_rt::test]
async fn test_server_is_up() {
    let mut app =
        test::init_service(App::new().wrap(Cors::default()).configure(setup_app_config)).await;

    let req = TestRequest::get().uri("/").to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(resp.status(), StatusCode::OK);
}

#[actix_rt::test]
async fn test_register() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = User {
        email: "user@example.com".to_string(),
        password: "password".to_string(),
    };

    let req = TestRequest::post()
        .uri("/register")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert_eq!(
        resp.response().body().as_str(),
        "\"Could not connect to the User DB\""
    );
}

#[actix_rt::test]
async fn test_last_logout() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let req = TestRequest::post()
        .uri("/last_logout/3")
        .header("Origin", "https://www.unknown.com")
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::BAD_REQUEST);
    assert_eq!(
        resp.response().body().as_str(),
        "Origin is not allowed to make this request"
    );

    let req = TestRequest::post()
        .uri("/last_logout/3")
        .header("Origin", "http://auth-service:4000")
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert_eq!(
        resp.response().body().as_str(),
        "\"Could not connect to the User DB\""
    );
}

#[actix_rt::test]
async fn test_validate() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = User {
        email: "user@example.com".to_string(),
        password: "password".to_string(),
    };

    let req = TestRequest::post()
        .uri("/validate")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::BAD_REQUEST);
    assert_eq!(
        resp.response().body().as_str(),
        "Origin is not allowed to make this request"
    );

    let req = TestRequest::post()
        .uri("/validate")
        .header("Origin", "http://auth-service:4000")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert_eq!(
        resp.response().body().as_str(),
        "\"Could not connect to the User DB\""
    );
}

#[actix_rt::test]
async fn test_sign_out() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let req = TestRequest::post()
        .uri("/sign_out/3")
        .header("Origin", "https://www.unknown.com")
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::BAD_REQUEST);
    assert_eq!(
        resp.response().body().as_str(),
        "Origin is not allowed to make this request"
    );

    let req = TestRequest::post()
        .uri("/sign_out/3")
        .header("Origin", "http://auth-service:4000")
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert_eq!(
        resp.response().body().as_str(),
        "\"Could not connect to the User DB\""
    );
}

#[actix_rt::test]
async fn test_change_password() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = ChangePassword {
        token: "token".to_string(),
        old_password: "password".to_string(),
        new_password: "new_password".to_string(),
    };

    let req = TestRequest::post()
        .uri("/change_password/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_delete() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = Token {
        token: "token".to_string(),
    };

    let req = TestRequest::delete()
        .uri("/delete/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_get_profile() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = Token {
        token: "token".to_string(),
    };

    let req = TestRequest::post()
        .uri("/get_profile/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_set_profile() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = SetProfile {
        token: "token".to_string(),
        firstname: Some("firstname".to_string()),
        lastname: None,
    };

    let req = TestRequest::post()
        .uri("/set_profile/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_get_firstname() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = Token {
        token: "token".to_string(),
    };

    let req = TestRequest::post()
        .uri("/firstname/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_get_address() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = Token {
        token: "token".to_string(),
    };

    let req = TestRequest::post()
        .uri("/get_address/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}

#[actix_rt::test]
async fn test_set_address() {
    let mut app = test::init_service(
        App::new()
            .wrap(Cors::default())
            .data(setup_postgres_config())
            .configure(setup_app_config),
    )
    .await;

    let params = SetAddress {
        token: "token".to_string(),
        fullname: "Some Name".to_string(),
        street: "Some Street 123".to_string(),
        city: "Some City".to_string(),
        postcode: "Some Postcode 123".to_string(),
        country: "Some Country".to_string(),
        phone: "Some Phone 123".to_string(),
    };

    let req = TestRequest::post()
        .uri("/set_address/3")
        .header("Origin", "https://www.unknown.com")
        .set_json(&params)
        .to_request();
    let resp: ServiceResponse = app.call(req).await.unwrap();
    assert_eq!(resp.status(), StatusCode::INTERNAL_SERVER_ERROR);
    assert!(resp
        .response()
        .body()
        .as_str()
        .starts_with("\"Could not validate token through auth-service:"));
}
