# user-service

User service for the bachelor thesis.

Implementation in Rust, using actix-web crates. The database implementation in PostgreSQL, using tokio_postgres and deadpool_postgres crates.

Note that the user service runs as async HttpServer on "localhost:6002", but when used in a whole Microservices project, the docker reroutes to "user-service:6002". The DB runs on "localhost:5432", and similar as user-service is rerouted to "userdb:6001".

Requirements:

- Docker: https://www.docker.com/products

To run, call `docker-compose up`.

- userdb runs on port `6001`.
- user-service runs on port `6002`.

## API

---
**GET "/"**

A simple hello from the server.

Possible responses:
* 200 if OK and a body with a hello message.

---
**POST "/register"**

New user registration (adding to the user DB).

Expected Request body:
```
    email: String,
    password: String,
```
Possible responses:
* 500 if an error occured, 
* 409 if email already in use, 
* 200 if OK and a body with a newly asigned user id:
```
    user_id: i32,
```

---
**POST "/validate"**

User email and password validation (checking if exists in the user DB).

Blocks all origins, except `auth-service`.

Expected Request body:
```
    email: String,
    password: String,
```
Possible responses:
* 500 if an error occured, 
* 401 if email or password incorrect, 
* 200 if OK and a body with a user id:
```
    user_id: i32
```

---
**POST "/change_password/{user_id}"**

Change user password in the user DB.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
    old_password: String,
    new_password: String,
```
Possible responses:
* 500 if an error occured, 
* 401 if token, user id or password incorrect, 
* 200 if OK and a body with a string of confirmation.

---
**POST "/sign_out/{user_id}"**

Sign out user. Set last_logout in the DB.

Blocks all origins, except `auth-service`.

Possible responses:
* 500 if an error occured, 
* 409 if user id is invalid, 
* 200 if OK and a body with a string of confirmation.

---
**POST "/delete/{user_id}"**

Delete user from the DB. 

Calls `cart-service` for user deletion as well.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String
```
Possible responses:
* 500 if an error occured, 
* 401 if token or user id is invalid,
* 409 if user does not exist in the DB,
* 200 if OK and a body with a string of confirmation.

---
**POST "/last_logout/{user_id}"**

Returns last_logout time stamp.

Blocks all origins, except `auth-service`.

Possible responses:
* 500 if an error occured, 
* 409 if user id is invalid, 
* 200 if OK and a body with a last logout timestamp:
```
    last_logout: timestamp,
```

---
**POST "/get_profile/{user_id}"**

Returns user profile from the DB.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
```
Possible responses:
* 500 if an error occured,
* 401 if token is invalid,
* 409 if user id is invalid, 
* 200 if OK and a body with a user profile:
```
    email: String,
    firstname: Option<String>,
    lastname: Option<String>,
    created_on: DateTime<Utc>,
```

---
**POST "/set_profile/{user_id}"**

Sets user profile in the DB.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
    firstname: Option<String>,
    lastname: Option<String>,
```
Possible responses:
* 500 if an error occured, 
* 401 if token is invalid,
* 409 if user id is invalid,
* 200 if OK and a body with a string of confirmation.

---
**POST "/get_firstname/{user_id"}**

Returns user's first name as string from the DB.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
```
Possible responses:
* 500 if an error occured, 
* 401 if token is invalid,
* 409 if user id is invalid,
* 200 if OK and a body with a user's firstname:
```
    firstname: Option<String>,
```

---
**POST "/get_address/{user_id}"**

Returns user's address from the DB. Empty if not found.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
```
Possible responses:
* 500 if an error occured,
* 401 if token is invalid,
* 200 if OK and a body with a user's address:
```
    user_id: i32,
    fullname: String,
    street: String,
    city: String,
    postcode: String,
    country: String,
    phone: String
```

---
**POST "/set_address/{user_id}"**

Sets user's address in the DB.

Calls `auth-service` for token validation.

Expected Request body:
```
    token: String,
    fullname: String,
    street: String,
    city: String,
    postcode: String,
    country: String,
    phone: String
```
Possible responses:
* 500 if an error occured,
* 401 if token is invalid,
* 409 if user id is invalid,
* 200 if OK and a body with a string of confirmation.