# user-service

Shipment service for the bachelor thesis.

Requirements:

- Docker: https://www.docker.com/products

To run, call `docker-compose up`.

- shipment-service runs on port `9000`.
- shipment-db runs on port `9001`.

Shipment service is implemented in Python.
Shipment database uses Mongodb.

To access shipment service documentation GUI use this link: http://127.0.0.1:9000/docs.

## API

---
**GET /api/{user_id}/{order_id}**
Expected Request body
```
    token: String,
```
Possible return responses: 
* 500 if an error occured, 
* 401 if token is invalid, 
* 200 if OK

---
**POST api/ship/{user_id}/{order_id}/{status} expects**
Expected Request body
```
    token: String,
```
Possible return responses: 
* 500 if an error occured, 
* 401 if token is invalid, 
* 200 if OK

---
**GET /**
Possible return responses: 
* 500 if an error occured,
* 200 if OK and a message that the service is running.