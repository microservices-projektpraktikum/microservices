import os
import logging
import requests
import uvicorn
import json
import datetime
from enum import Enum
from databases import DatabaseURL
from fastapi import FastAPI, HTTPException, Request, APIRouter, Depends, Path
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from motor.motor_asyncio import AsyncIOMotorClient

from typing import List

# Setup constants
logging.basicConfig(level=logging.INFO)

MONGO_URL = DatabaseURL("mongodb://admin:password@shipmentdb:27017")
HEADERS = {'Content-type': 'application/json'}
HTTP_400_BAD_REQUEST = 400
HTTP_401_Unauthorized = 401
HTTP_500_Internal = 500


# Database
client = AsyncIOMotorClient(str(MONGO_URL), maxPoolSize=15,minPoolSize=15)
# Router
router = APIRouter()

''' 
Status of the shipment
'''
class Status(Enum):
    PROCESSING = 'processing'
    DISPATCHED = 'dispatched'
    TRANSIT = 'transit'
    DELIVERED = 'delivered'
    FAIL = 'fail'

class Address(BaseModel):
    fullname: str
    street: str
    city: str
    postcode: str
    country: str
    phone: str

class Shipment(BaseModel):
    id: str
    order_id: str
    status: str
    datetime: datetime.datetime
    address: Address

class Shipments(BaseModel):
    shipments: List[Shipment]

class ShipmentId(BaseModel):
    id: str

class Body(BaseModel):
    token: str
    address: Address

class TokenBody(BaseModel):
    token: str

class CustomException(Exception):
    def __init__(self, status_code: int, msg: str):
        self.status_code = status_code
        self.msg = msg

''' 
Gets Mongo DB
'''
async def get_database() -> AsyncIOMotorClient:
    logging.info(str(MONGO_URL))
    return client

''' 
Closes opened connection to the server's Mongo DB
'''
async def close_mongo_connection():
    logging.info("Closing Connection to the Shipment DB...")
    client.close()
    logging.info("Connection to the Shipment DB Closed.")

''' 
Gets DB collection
'''
def get_collection(client: AsyncIOMotorClient):
    if client is None:
        raise CustomException(HTTP_500_Internal, "Shipment database is down.")
    db = client.get_database("shipmentdb")
    collection = db.get_collection("shipment")
    return collection

''' 
Calls auth-service for token validation
'''
async def validate_token(token: str, user_id: str):
    try:
        data = {'token': token}
        response = requests.post("http://auth-service:4000/isTokenValid", headers=HEADERS, data=json.dumps(data))
        response_json = response.json()
        if response.status_code != 200 or not response_json["valid"]:
            logging.info(f"Invalid token for user {user_id}: {token}")
            raise CustomException(HTTP_401_Unauthorized, "Invalid token")
        token_user_id = response_json["user_id"]
        if int(token_user_id) != int(user_id):
            raise CustomException(HTTP_401_Unauthorized, f"User {token_user_id} trying to ship for another user {user_id}.")
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        logging.info(f"I ended up here. {e}")
        raise CustomException(HTTP_500_Internal, f"Could not validate token: {e}")

''' 
Returns shipment from the DB by the order id
'''
async def get_shipment(conn: AsyncIOMotorClient, order_id: str):
    coll = get_collection(conn)
    return await coll.find_one({"order_id": order_id})

''' 
Saves shipment data in the DB
'''
async def ship(conn: AsyncIOMotorClient, user_id: str, order_id: str, status: Status, address: Address):
    coll = get_collection(conn)
    shipment = await coll.find_one({"order_id": order_id})
    if not shipment:
        return await coll.insert_one(
            {
                "user_id": user_id,
                "order_id": order_id,
                "status": status.value,
                "datetime": datetime.datetime.now(datetime.timezone.utc),
                "address": json.dumps(address, default=dict),
            })
    else:
        # This will update only status and datetime stamp. Will not update address
        myquery = {"order_id": order_id}
        newvalues = {"$set": {"status": status.value,
                            "datetime":  datetime.datetime.now(datetime.timezone.utc)}}
        return await coll.update_one(myquery, newvalues)

''' 
Retrieves a shipment by order_id.
Since Order id is unique, it is used as shipment id as well.
'''
@router.post("/get_shipment/{user_id}/{order_id}", response_model=Shipment)
async def retrieve_shipment(
    user_id: str = Path(..., min_length=1),
    order_id: str = Path(..., min_length=1),
    data: TokenBody = None,
    db: AsyncIOMotorClient = Depends(get_database),
):
    logging.info(f"Getting shipment for user {user_id} with order {order_id}")
    await validate_token(data.token, user_id)
    shipment = await get_shipment(db, order_id)
    address = json.loads(shipment["address"])
    return Shipment(
        id=str(shipment["_id"]), 
        order_id=shipment["order_id"],
        status=shipment["status"],
        datetime=shipment["datetime"],
        address=Address(
            fullname=address["fullname"],
            street=address["street"],
            city=address["city"],
            postcode=address["postcode"],
            country=address["country"],
            phone=address["phone"]
        )
    )
    


'''
Adds or updates a shipment with order_id as id and a status for demonstration purposes.
This should not have status in real application when adding a new shipment.
'''
@router.post("/ship/{user_id}/{order_id}/{status}", response_model=ShipmentId)
async def add_shipment(
    user_id: str = Path(..., min_length=1),
    order_id: str = Path(..., min_length=1),
    status: str = Path(..., min_length=1),
    data: Body = None,
    db: AsyncIOMotorClient = Depends(get_database),
):
    logging.info(
        f"Posting shipment for user {user_id} with order {order_id} and status {status}")
    await validate_token(data.token, user_id)
    state = Status.FAIL
    try:
        state = Status(status)
    except Exception as e:
        logging.info(e)
        raise CustomException(HTTP_400_BAD_REQUEST, f"Invalid shipment status: '{status}'")
    # For project demonstration only
    if state == Status.FAIL:
        raise CustomException(HTTP_500_Internal, "Deliberate shipment failure.")
    shipment = await ship(db, user_id, order_id, state, data.address)
    logging.info('Inserted shipment %s' % shipment.inserted_id)
    return ShipmentId(id=str(shipment.inserted_id))

'''
For development purposes only
'''
@router.get("/all", response_model=List[Shipment])
async def all(db: AsyncIOMotorClient = Depends(get_database)):
    logging.info("Returning all shipments")
    logging.info(db.address)
    coll = get_collection(db)
    cursor = coll.find()
    items = await cursor.to_list(length=500)
    # do some conversion, since _id is private and would not be printed
    for i in items:
        i['shipment_id'] = str(i.pop('_id'))
    return convert_to_response(Shipments(shipments=items))

'''
Simple server greeting
'''
@router.get('/')
async def index():
    msg = "Shipment service is up!"
    logging.info(msg)
    return {"msg": msg}

'''
Response converter from BaseModel to json
'''
def convert_to_response(model: BaseModel) -> JSONResponse:
    return JSONResponse(content=jsonable_encoder(model, by_alias=True))

'''Main App'''
app = FastAPI(title="Shipment App")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_event_handler("shutdown", close_mongo_connection)
app.include_router(router, prefix="/api")

''' 
Custom exception handler
'''
@app.exception_handler(CustomException)
def custom_exception_handler(request: Request, exc: CustomException):
    return JSONResponse(
        status_code = exc.status_code,
        content={"message": exc.msg},
    )

# Start Server
if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000, reload=True)